package com.homejerrrider

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import com.homejerrrider.databinding.ActivityMainBinding
import com.homejerrrider.sharedpreference.PreferenceFile
import com.homejerrrider.utils.CommonAlerts.loadFragment
import com.homejerrrider.utils.CommonMethods
import com.homejerrrider.utils.HomeInterface
import com.homejerrrider.utils.openActivity
import com.homejerrrider.views.documentic.DocumentICVM
import com.homejerrrider.views.fragments.acceptOrder.AcceptedOrderVM
import com.homejerrrider.views.fragments.addNewCard.NewCardVM
import com.homejerrrider.views.fragments.chatFragment.ChatVM
import com.homejerrrider.views.fragments.completeOrder.CompleteOrderVM
import com.homejerrrider.views.fragments.contactFragment.ContactFragment
import com.homejerrrider.views.fragments.contactFragment.ContactVM
import com.homejerrrider.views.fragments.drivinglicense.DrivingLicenseVM
import com.homejerrrider.views.fragments.earningFragment.EarningFragment
import com.homejerrrider.views.fragments.earningFragment.EarningVM
import com.homejerrrider.views.fragments.editprofile.EditProfile
import com.homejerrrider.views.fragments.editprofile.EditProfileVM
import com.homejerrrider.views.fragments.homeFragment.HomeFragment
import com.homejerrrider.views.fragments.homeFragment.HomefragmentVM
import com.homejerrrider.views.fragments.myDocument.MyDocumentFragment
import com.homejerrrider.views.fragments.myDocument.MyDocumentVM
import com.homejerrrider.views.fragments.notificationFragment.NotificationFragment
import com.homejerrrider.views.fragments.notificationFragment.NotificationVM
import com.homejerrrider.views.fragments.orderAccepted.OrderAcceptedVM
import com.homejerrrider.views.fragments.orderIDFragment.OrderIdVM
import com.homejerrrider.views.fragments.paymentFragment.PaymentVM
import com.homejerrrider.views.fragments.processOrder.ProcessingOrderVM
import com.homejerrrider.views.fragments.supportFragment.SupportFragment
import com.homejerrrider.views.fragments.supportFragment.SupportVM
import com.homejerrrider.views.fragments.track.TrackVM
import com.homejerrrider.views.fragments.walletFragment.WalletFragment
import com.homejerrrider.views.fragments.walletFragment.WalletVM
import com.homejerrrider.views.login.SigninActivity
import com.homejerrrider.views.vehicleregistrations.VehicleRegisterVM
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), HomeInterface {
    companion object {
        var binding: ActivityMainBinding? = null
        fun closeDrawer() {
            binding?.drawerLayout?.closeDrawer(GravityCompat.START)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        loadFragment(HomeFragment(), null)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        homeInitialize()
        clicks()
    }

    private fun homeInitialize() {
        ContactVM.homeInterface = this
        SupportVM.homeInterface = this
        HomefragmentVM.homeInterface = this
        ContactVM.homeInterface = this
        ChatVM.homeInterface = this
        SupportVM.homeInterface = this
        EditProfileVM.homeInterface = this
        ProcessingOrderVM.homeInterface = this
        AcceptedOrderVM.homeInterface = this
        DocumentICVM.homeInterface = this
        OrderIdVM.homeInterface = this
        DrivingLicenseVM.homeInterface = this
        VehicleRegisterVM.homeInterface = this
        WalletVM.homeInterface = this
        CompleteOrderVM.homeInterface = this
        PaymentVM.homeInterface = this
        NewCardVM.homeInterface = this
        /*added by vikas*/
        MyDocumentVM.homeInterface = this
        EarningVM.homeInterface = this
        NotificationVM.homeInterface = this
        OrderAcceptedVM.homeInterface = this
        AcceptedOrderVM.homeInterface = this
        TrackVM.homeInterface = this
    }

    fun clicks() {
        binding?.llHome?.setOnClickListener { loadFragment(HomeFragment(), null);closeDrawer() }
        binding?.llProfile?.setOnClickListener { loadFragment(EditProfile(), null);closeDrawer() }
        binding?.llDocs?.setOnClickListener {
            loadFragment(
                MyDocumentFragment(),
                null
            );closeDrawer()
        }
        binding?.llEarning?.setOnClickListener {
            loadFragment(
                EarningFragment(),
                null
            );closeDrawer()
        }
        binding?.llWallet?.setOnClickListener { loadFragment(WalletFragment(), null);closeDrawer() }
        binding?.llNoti?.setOnClickListener {
            loadFragment(
                NotificationFragment(),
                null
            );closeDrawer()
        }
        binding?.llContact?.setOnClickListener {
            loadFragment(
                ContactFragment(),
                null
            );closeDrawer()
        }
        binding?.txtSupport?.setOnClickListener {
            loadFragment(
                SupportFragment(),
                null
            );closeDrawer()
        }
        binding?.llLogout?.setOnClickListener { showLogout();closeDrawer() }
        binding?.cross?.setOnClickListener { closeDrawer() }

    }

    private fun showLogout() {
        val aD = AlertDialog.Builder(this)
        aD.setTitle(getString(R.string.logout_message))
        aD.setCancelable(false)
        aD.setPositiveButton(getString(R.string.ok)) { dialogInterface, _ ->
            PreferenceFile.clearPreference(this)
            CommonMethods.showToast(this, "Log Out Successfully Done")
            dialogInterface.cancel()
            dialogInterface.dismiss()
            openActivity(Intent(this, SigninActivity::class.java), true)
        }
        aD.setNegativeButton(getString(R.string.cancel)) { dialogInterface, _ ->
            dialogInterface.cancel()
            dialogInterface.dismiss()
        }
        aD.create()
        aD.show()
    }

    private fun openCloseDrawer() {
        if (drawerLayout!!.isDrawerOpen(GravityCompat.START)) {
            drawerLayout!!.closeDrawer(GravityCompat.START)
        } else {
            drawerLayout!!.openDrawer(GravityCompat.START)
        }
    }

    override fun onBackPressed() {
        if (this.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            val manager = supportFragmentManager
            if (manager.backStackEntryCount == 1) {
                finish()
            } else {
                super.onBackPressed()
            }
        }
    }

    override fun backPress() {
        onBackPressed()
    }

    override fun homeToggle() {
        openCloseDrawer()
    }

    override fun drawerLock(boolean: Boolean) {
    }

    override fun onBackPress() {
        onBackPressed()
    }

}