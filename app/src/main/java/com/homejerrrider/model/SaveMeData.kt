package com.homejerrrider.model

data class SaveMeData(
    var email: String ="",
    var phone: String ="",
    var countryCode: String ="+91",
    var password: String ="",
    var emailPassword: String ="",
    var isRemember: Boolean = false

)