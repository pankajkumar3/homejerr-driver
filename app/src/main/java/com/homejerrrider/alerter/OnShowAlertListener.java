package com.homejerrrider.alerter;

public interface OnShowAlertListener {
    /**
     * Called when the Alert is hidden
     */
    void onShow();
}
