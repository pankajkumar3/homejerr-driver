package com.homejerrrider.alerter;

public interface OnHideAlertListener {
    /**
     * Called when the Alert is initially Shown
     */
    void onHide();
}
