package com.homejerrrider.recyclerAdapter

abstract class AbstractModel {

    var adapterPosition: Int = -1
    var onItemClick: RecyclerAdapter.OnItemClick? = null
}