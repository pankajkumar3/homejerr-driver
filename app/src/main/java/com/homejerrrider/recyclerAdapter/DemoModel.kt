package com.homejerrrider.recyclerAdapter

import android.graphics.drawable.Drawable
import android.net.Uri

data class ItemDocModel(
    var name: String = "ll",
    var path: String,
    var isSelected: Boolean = false,
    var uri: Uri?= null
) : AbstractModel()
data class ItemVehicleRegModel(
    var name: String = "ll",
    var path: String,

    var isSelected: Boolean = false,
    var uri: Uri? = null
) : AbstractModel()
data class ItemIcModel(
    var name: String = "ll",
    var path: String,

    var isSelected: Boolean = false,
    var uri: Uri? = null
) : AbstractModel()


