package com.homejerrrider.utils

import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableField
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.github.angads25.toggle.LabeledSwitch
import com.github.angads25.toggle.interfaces.OnToggledListener
import com.google.android.material.tabs.TabLayout
import com.hbb20.CountryCodePicker
import com.homejerrrider.R
import com.homejerrrider.networkCalls.WebApiKey
import com.mukesh.mukeshotpview.completeListener.MukeshOtpCompleteListener
import com.mukesh.mukeshotpview.mukeshOtpView.MukeshOtpView
import de.hdodenhof.circleimageview.CircleImageView

object BindingAdapters {
    @BindingAdapter(value = ["setToggleListener"], requireAll = false)
    @JvmStatic
    fun setToggleListener(
        toggle: LabeledSwitch,
        listener: OnToggledListener
    ) {
        toggle.setOnToggledListener(listener)
    }
    @BindingAdapter(value = ["viewPagerAdapter", "setupDotsIndicator"], requireAll = false)
    @JvmStatic
    fun setViewPagerAdapter(viewPager: ViewPager, adapter: PagerAdapter, dotsIndicator: TabLayout) {
        viewPager.clipToPadding = false
        viewPager.setPageTransformer(false) { page, position ->
            page.translationX = -(0.25f * position)
            page.scaleY = 1 - (0.25f * kotlin.math.abs(position))
        }
        viewPager.adapter = adapter
        dotsIndicator.setupWithViewPager(viewPager)
    }

    @BindingAdapter(value = ["setRecyclerAdapter"], requireAll = false)
    @JvmStatic
    fun setRecyclerAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>) {
        recyclerView.adapter = adapter
    }

    @BindingAdapter(value = ["setSelectedCountryCode"], requireAll = false)
    @JvmStatic
    fun setSelectedCountryCode(
        countryCodePicker: CountryCodePicker,
        observableField: ObservableField<Int>
    ) {
        countryCodePicker.setCountryForPhoneCode(observableField.get()!!)
    }


    @BindingAdapter(value = ["otpListener"], requireAll = false)
    @JvmStatic
    fun otpListener(
        otpView: MukeshOtpView,
        listener: MukeshOtpCompleteListener
    ) {
        otpView.setOtpCompletionListener(listener)
    }
    @BindingAdapter(value = ["setImage"], requireAll = false)
    @JvmStatic
    fun setImage(circleImageView: CircleImageView, observableField: ObservableField<String>) {
        if (observableField.get() != "") {
            Glide.with(circleImageView.context!!).load(WebApiKey.BASE_URL_IMAGE + observableField.get())
                .placeholder(circleImageView.context!!.resources.getDrawable(R.drawable.dummy_noti))
                .into(circleImageView)
        }
    }
}