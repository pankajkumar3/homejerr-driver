package com.homejerrrider.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import com.homejerrrider.R
import com.homejerrrider.alerter.Alerter
import com.homejerrrider.validate.ValidatorUtils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

fun Context.openActivity(intent: Intent, finishAffinity: Boolean) {

    ValidatorUtils.hideSoftKeyboard(this as Activity)
    startActivity(intent)
//    this.overridePendingTransition(R.anim.slide_up, R.anim.slide_up)
    if (finishAffinity) this.finishAffinity()
}
    /* Internet Connection Detector */
    fun Context.isNetworkAvailable(): Boolean {
        val service = Context.CONNECTIVITY_SERVICE
        val manager = getSystemService(service) as ConnectivityManager?
        val network = manager?.activeNetworkInfo
        return (network != null)
    }

    /* Positive Alerter*/
    fun Context.showNegativeAlerter(message: String) {
        /* Snackbar.make(
             (this as Activity).findViewById(android.R.id.content),
             message,
             Snackbar.LENGTH_SHORT
         ).show()*/
        Alerter.create(this as Activity)
                .setTitle(message)
                .setBackgroundColorRes(R.color.alert_default_error_background)
                .show()
    }

    /* Negative Alerter*/
    fun Context.showPositiveAlerter(message: String) {
        Alerter.create(this as Activity)
                .setTitle(message)
                .setBackgroundColorRes(R.color.alerter_default_success_background)
                .show()
    }
/*string to part request body*/
fun getPartRequestBody(string: String): RequestBody {
    return string
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
}
fun getPartFromFile(string: String, param: String): MultipartBody.Part {
    val file = File(string)
    val reqFile = file.asRequestBody("image/jpeg".toMediaTypeOrNull())
    return MultipartBody.Part.createFormData(param, file.name, reqFile)
}

