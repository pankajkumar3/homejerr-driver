package com.homejerrrider.utils

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.snackbar.Snackbar
import com.homejerrrider.R

object CommonAlerts {
    fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun Context.loadFragment(fragment: Fragment, bundle: Bundle?) {

        if (bundle != null) {
            fragment.arguments = bundle
        }
        val fragmentManager =
            (this as FragmentActivity).supportFragmentManager.beginTransaction()
        fragmentManager
            /* .setCustomAnimations(
                 R.anim.enter,
                 R.anim.exit,
                 R.anim.pop_enter,
                 R.anim.pop_exit
             )*/
            .replace(R.id.frame, fragment)
            .addToBackStack(null)
            .commit()
    }

    fun loadFragmentWithBundle(fragment: Fragment, bundle: Bundle?) {

        if (bundle != null) {
            fragment.arguments = bundle
        }
        val fragmentManager = (this as FragmentActivity).supportFragmentManager.beginTransaction()
        fragmentManager
            /* .setCustomAnimations(
                 R.anim.enter,
                 R.anim.exit,
                 R.anim.pop_enter,
                 R.anim.pop_exit
             )*/
            .replace(R.id.frame, fragment)
            .addToBackStack(null)
            .commit()
    }

    fun loadFragment(context: Context, fragment: Fragment) {
        (context as FragmentActivity)
        val manager = context.supportFragmentManager
        val ft: FragmentTransaction = manager.beginTransaction()
            .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
        val list = context.supportFragmentManager.fragments
        if (list.contains(fragment)) {
            manager.popBackStack(fragment.javaClass.name, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
        ft.replace(R.id.frame, fragment)
            .addToBackStack(fragment.javaClass.name)
            .commit()
    }

    fun showSnackbar(message: String, context: Context) {
        if (message.isNotEmpty()) {
            Snackbar.make(
                (context as Activity).findViewById(android.R.id.content),
                message,
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }
}