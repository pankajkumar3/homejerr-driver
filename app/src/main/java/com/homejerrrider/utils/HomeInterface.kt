package com.homejerrrider.utils

interface HomeInterface{
    fun backPress()
    fun homeToggle()
    fun drawerLock(boolean : Boolean)
    fun onBackPress()
}
