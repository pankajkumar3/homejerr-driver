package com.homejerrrider.utils

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import com.homejerrrider.databinding.AlertLayoutBinding
import com.homejerrrider.databinding.ProgressLayoutBinding
import com.homejerrrider.views.login.SigninActivity

//Loader

private var customDialog: AlertDialog? = null

fun Context.showDialog() {
    hideDialog()
    val customAlertBuilder = AlertDialog.Builder(this)
    val customAlertView = ProgressLayoutBinding.inflate(LayoutInflater.from(this), null, false)
    customAlertBuilder.setView(customAlertView.root)
    customAlertBuilder.setCancelable(false)
    customDialog = customAlertBuilder.create()
    customDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    customDialog!!.show()
}

fun Context.hideDialog() {
    if (customDialog != null && customDialog?.isShowing!!) {
        customDialog?.dismiss()
    }
}
//ErrorAlert

fun Context.alert(title: String, message: String) {
    try {
        val builder = AlertDialog.Builder(this)
        val layoutView = AlertLayoutBinding.inflate(LayoutInflater.from(this), null, false)
        builder.setCancelable(false)
        builder.setView(layoutView.root)
        val dialog = builder.create()

        layoutView.tvAlertHeading.text = title
        layoutView.tvMessage.text = message
        layoutView.tvOkButton.setOnClickListener {
            dialog.dismiss()
        }

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

//ErrorTokenAlert

fun Context.alertToken(title: String, message: String, context: Context) {
    try {
        val builder = AlertDialog.Builder(this)
        val layoutView = AlertLayoutBinding.inflate(LayoutInflater.from(this), null, false)
        builder.setCancelable(false)
        builder.setView(layoutView.root)
        val dialog = builder.create()

        layoutView.tvAlertHeading.text = title
        layoutView.tvMessage.text = message
        layoutView.tvOkButton.setOnClickListener {

            /* val editor = context.getSharedPreferences("com.convoice", Context.MODE_PRIVATE).edit()

             editor.putString(PreferenceKeys.FName,"")
             editor.putString(PreferenceKeys.LName,"")
             editor.putString(PreferenceKeys.EMAIL,"")
             editor.putString(PreferenceKeys.GENDER,"")
             editor.putString(PreferenceKeys.NUMBER,"")
             editor.putString(PreferenceKeys.COUNTRY,"")
             editor.putString(PreferenceKeys.ID,"")
             editor.putString(PreferenceKeys.TOKEN,"")
             editor.putString(PreferenceKeys.ISSOCIAL,"no")

             editor.apply()*/

            startActivity(Intent(context, SigninActivity::class.java))
            (context as Activity).finishAffinity()

            dialog.dismiss()
        }

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}