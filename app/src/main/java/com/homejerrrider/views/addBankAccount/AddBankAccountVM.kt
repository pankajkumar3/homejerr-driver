package com.homejerrrider.views.addBankAccount

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.homejerrrider.utils.CommonAlerts.loadFragmentWithBundle
import com.homejerrrider.views.fragments.editprofile.EditProfile

class AddBankAccountVM(val context: Context, val intent: Intent) : ViewModel() {
    var phone = ObservableField("")
    var bankName = ObservableField("")
    var isEdit = ObservableBoolean(false)
    fun onClicks(value: String) {
        when (value) {
            "add" -> {

                if (isEdit.get()) {
                    val bundle = Bundle()
                    bundle.apply {
                        putString("bank", bankName.get())
                        putString("phone", phone.get())
                    }

                    loadFragmentWithBundle(EditProfile(), bundle)

                } else {
                    val intent = Intent()
                    intent.putExtra("phone", phone.get())
                    intent.putExtra("bank", bankName.get())
                    (context as Activity).setResult(Activity.RESULT_OK, intent)
                    context.finish()

                }
                // (context as AddBankAccountActivity).startActivity(Intent(context, ProfileSetupActivity::class.java))
            }
            "cross" -> {
                (context as AddBankAccountActivity).onBackPressed()
            }

            "back" -> {
                (context as AddBankAccountActivity).onBackPressed()
            }
        }
    }

    /*  fun get()
      {
          if (intent.hasExtra("profile"))
          {
              (context as AddBankAccountActivity).startActivity(Intent(context,ProfileSetupActivity::class.java).putExtra("bank","profile"))

          }
      }*/
}