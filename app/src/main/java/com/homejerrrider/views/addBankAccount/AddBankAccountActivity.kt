package com.homejerrrider.views.addBankAccount

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.homejerrrider.R
import com.homejerrrider.databinding.ActivityAddBankAccountBinding
import kotlinx.android.synthetic.main.activity_add_bank_account.*


class AddBankAccountActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    var bankList = ArrayList<String>()
    var viewModel: AddBankAccountVM? = null
    private var binding: ActivityAddBankAccountBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_bank_account)
        val factory = AddBankAccountFactory(this, intent)
        viewModel = ViewModelProvider(this, factory).get(AddBankAccountVM::class.java)
        binding?.vm = viewModel
        getData()
        setBankData()

    }

    private fun getData() {
        if (intent.hasExtra("comes") && intent.getStringExtra("comes").equals("edit")) {
            viewModel?.isEdit!!.set(true)
        } else {
            viewModel?.isEdit!!.set(false)
        }
    }

    private fun setBankData() {
        spBank.onItemSelectedListener = this
        bankList.add("State Bank of India")
        bankList.add("Punjab National Bank")
        bankList.add("HDFC")
        val aa = ArrayAdapter(this, android.R.layout.simple_selectable_list_item, bankList)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spBank!!.adapter = aa
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        viewModel!!.bankName.set(bankList[position])
    }
}