package com.homejerrrider.views.addBankAccount

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AddBankAccountFactory (val context: Context, val intent: Intent): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AddBankAccountVM::class.java)) {
            return AddBankAccountVM(context, intent) as T
        }
        throw IllegalArgumentException("")
    }
}