package com.homejerrrider.views.fragments.myDocument

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.homejerrrider.MainActivity
import com.homejerrrider.utils.CommonAlerts
import com.homejerrrider.utils.HomeInterface
import com.homejerrrider.views.documentic.DocumentIC
import com.homejerrrider.views.fragments.drivinglicense.DrivingLicense
import com.homejerrrider.views.vehicleregistrations.VehicleRegister

class MyDocumentVM(val context: Context) : ViewModel() {

    companion object {
        var homeInterface: HomeInterface? = null
    }

    fun onClicks(value: String) {

        when (value) {
            "backFromMyDoc" -> {
                (context as MainActivity).startActivity(Intent(context, MainActivity::class.java))
            }
            "FromDocumentLicense" -> {
                CommonAlerts.loadFragment(context, DrivingLicense())
            }
            "fromDocumentVehicleRegister" -> {
                CommonAlerts.loadFragment(context, VehicleRegister())
            }"fromDocumentIcToIC" -> {
                CommonAlerts.loadFragment(context, DocumentIC())
            }
        }
    }
}