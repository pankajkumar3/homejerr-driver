package com.homejerrrider.views.fragments.supportFragment

import android.content.Context
import androidx.lifecycle.ViewModel
import com.homejerrrider.utils.CommonAlerts
import com.homejerrrider.utils.HomeInterface
import com.homejerrrider.views.fragments.contactFragment.ContactFragment

class SupportVM(val context:Context):ViewModel() {
    companion object{
        var homeInterface: HomeInterface?=null
    }
    fun clicks(value:String)
    {
        when(value)
        {
            "contact"->
            {
                CommonAlerts.loadFragment(context,ContactFragment())
            }
            "drawer"->
            {
                homeInterface!!.homeToggle()
            }
        }
    }

}