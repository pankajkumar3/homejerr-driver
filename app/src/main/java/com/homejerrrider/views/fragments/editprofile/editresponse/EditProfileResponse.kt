package com.homejerrrider.views.fragments.editprofile.editresponse

data class EditProfileResponse(
    val `data`: Data,
    val message: String,
    val success: Boolean
)