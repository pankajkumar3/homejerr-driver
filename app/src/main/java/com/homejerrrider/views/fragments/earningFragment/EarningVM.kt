package com.homejerrrider.views.fragments.earningFragment

import android.content.Context
import androidx.lifecycle.ViewModel
import com.homejerrrider.utils.HomeInterface

class EarningVM(val context: Context):ViewModel() {
    companion object{
        var homeInterface:HomeInterface?=null
    }
    fun clicks(value:String) {
        when (value)
        {
            "drawer" ->
            {
                homeInterface!!.homeToggle()
            }
        }
    }
}