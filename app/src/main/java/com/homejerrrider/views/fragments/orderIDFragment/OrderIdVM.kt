package com.homejerrrider.views.fragments.orderIDFragment

import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.homejerrrider.utils.CommonAlerts
import com.homejerrrider.utils.HomeInterface
import com.homejerrrider.views.fragments.homeFragment.HomeFragment
import com.homejerrrider.views.fragments.orderIDFragment.adapter.AdapterOrderList
import com.homejerrrider.views.fragments.track.TrackFragment

class OrderIdVM(val context: Context,val type:String):ViewModel() {
    val codeBooleanOrderBtn=ObservableField(false)
    val codeBooleanBtnIgnore=ObservableField(true)
    init {
        if(type=="acceptTheOrder")
        {
            codeBooleanBtnIgnore.set(false)
        }

    }
    companion object{
        var homeInterface:HomeInterface?=null
    }
    var adapter=AdapterOrderList(context)
    fun clicks(value:String)
    {
        when(value)
        {
            "back"->
            {
                homeInterface!!.backPress()
            }
            "ignore"->
            {
                CommonAlerts.loadFragment(context,HomeFragment())
            }
            "pick"->
            {
                CommonAlerts.loadFragment(context,TrackFragment())
            }

        }
    }
}