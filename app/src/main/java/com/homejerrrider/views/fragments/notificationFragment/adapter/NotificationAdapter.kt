package com.homejerrrider.views.fragments.notificationFragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.homejerrrider.R
import kotlinx.android.synthetic.main.adapter_notification_items.view.*

class NotificationAdapter(val context: Context):RecyclerView.Adapter<NotificationAdapter.MyViewModel>() {
    class MyViewModel(itemView:View):RecyclerView.ViewHolder(itemView) {
        val tvNotiText=itemView.tvNotiText
        val llNoti=itemView.llNoti
        val ivNotiImage=itemView.ivNotiImage
        val tbNotiMsg=itemView.tbNotiMsg

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewModel {
        val layout=LayoutInflater.from(context).inflate(R.layout.adapter_notification_items,parent,false)
        return MyViewModel(layout)
    }

    override fun getItemCount(): Int {
        return 8
    }

    override fun onBindViewHolder(holder: MyViewModel, position: Int) {
        if (position < 2)
        {

            if (position == 0)
            {
                holder.tvNotiText.visibility = View.VISIBLE
                holder.tvNotiText.text = "Recent"
            }
            holder.llNoti.setBackgroundColor(context.resources.getColor(R.color.noti))
            holder.ivNotiImage.setImageResource(R.drawable.ic_dummy_noti1)
            holder.tbNotiMsg.setText("Thank you! Your transaction is completed")
        }
        else
        {
            if (position == 2)
            {
                holder.tvNotiText.visibility = View.VISIBLE
                holder.tvNotiText.text = "Older notification"
            }
            holder.ivNotiImage.setImageResource(R.drawable.dummy_noti)
            holder.llNoti.setBackgroundColor(context.resources.getColor(R.color.trans))
        }
    }
    }
