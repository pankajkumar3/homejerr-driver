package com.homejerrrider.views.fragments.drivinglicense

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.homejerrrider.databinding.DrivingLicenseBinding

class DrivingLicense : Fragment() {
    private var vm: DrivingLicenseVM?=null
    var binding: DrivingLicenseBinding?=null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DrivingLicenseBinding.inflate(inflater)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBind()
    }

    private fun getBind(){
        vm= DrivingLicenseVM(context!!)
       binding!!.vm=vm
    }
}