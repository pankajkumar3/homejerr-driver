package com.homejerrrider.views.fragments.notificationFragment

import android.content.Context
import androidx.lifecycle.ViewModel
import com.homejerrrider.utils.HomeInterface
import com.homejerrrider.views.fragments.notificationFragment.adapter.NotificationAdapter

class NotificationVM(val context: Context):ViewModel() {
    companion object{
        var homeInterface:HomeInterface?=null
    }
    var adapter=NotificationAdapter(context)
    fun clicks(value:String)
    {
        when(value)
        {
            "drawer"->
            {
                homeInterface!!.homeToggle()
            }
        }
    }
}