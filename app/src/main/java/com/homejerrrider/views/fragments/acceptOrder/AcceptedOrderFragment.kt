package com.homejerrrider.views.fragments.acceptOrder

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.homejerrrider.R
import com.homejerrrider.databinding.FragmentAcceptedOrderBinding
import com.homejerrrider.databinding.FragmentSupportBinding
import com.homejerrrider.views.fragments.supportFragment.SupportVM

class AcceptedOrderFragment : Fragment() {
    var bundle=Bundle()
    lateinit var vm: AcceptedOrderVM
    lateinit var binding: FragmentAcceptedOrderBinding
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding= FragmentAcceptedOrderBinding.inflate(inflater)
        return binding.root
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onbind()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bundle = arguments!!
    }
    private fun onbind(){
        vm= AcceptedOrderVM(context!!,bundle)
        binding.viewModel=vm
    }
}