package com.homejerrrider.views.fragments.completeOrder

import android.content.Context
import androidx.lifecycle.ViewModel
import com.homejerrrider.utils.CommonAlerts
import com.homejerrrider.utils.HomeInterface
import com.homejerrrider.views.fragments.earningFragment.EarningFragment
import com.homejerrrider.views.fragments.orderIDFragment.OrderIDFragment

class CompleteOrderVM(val context: Context):ViewModel() {
    companion object{
        var homeInterface:HomeInterface?=null
    }
    fun clicks(value:String) {
        when (value)
        {
            "back" ->
            {
                CommonAlerts.loadFragment(context, EarningFragment())
            }
            "orderID"->
            {
                CommonAlerts.loadFragment(context, OrderIDFragment())
            }
        }
    }
}

