package com.homejerrrider.views.fragments.orderIDFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.homejerrrider.R
import com.homejerrrider.databinding.FragmentOrderIDBinding
import com.homejerrrider.databinding.FragmentPaymentBinding
import com.homejerrrider.views.fragments.paymentFragment.PaymentVM


class OrderIDFragment : Fragment() {

    lateinit var vm: OrderIdVM
    lateinit var binding: FragmentOrderIDBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentOrderIDBinding.inflate(inflater)
        return binding.root
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        var type = "ratenow"
        if (arguments != null)
        {
            type = arguments!!.getString("type","acceptTheOrder")
        }
        onbind(type)
    }
    private fun onbind(type:String){
        vm= OrderIdVM(context!!,type)
        binding.viewModel=vm
    }
}