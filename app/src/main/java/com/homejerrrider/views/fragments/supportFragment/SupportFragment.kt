package com.homejerrrider.views.fragments.supportFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.homejerrrider.databinding.FragmentSupportBinding

class SupportFragment : Fragment() {
    lateinit var vm: SupportVM
    lateinit var binding: FragmentSupportBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentSupportBinding.inflate(inflater)
        return binding.root
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onbind()
    }
    private fun onbind(){
        vm= SupportVM(context!!)
        binding.viewModel=vm
    }
}