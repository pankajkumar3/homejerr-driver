package com.homejerrrider.views.fragments.paymentFragment

import android.content.Context
import androidx.lifecycle.ViewModel
import com.homejerrrider.utils.CommonAlerts
import com.homejerrrider.utils.HomeInterface
import com.homejerrrider.views.fragments.addNewCard.AddNewCardFragment

class PaymentVM(val context: Context):ViewModel() {
    companion object{
        var homeInterface:HomeInterface?=null
    }
    fun clicks(value:String)
    {
        when(value)
        {
            "back"->
            {
                homeInterface!!.backPress()
            }
            "add"->
            {
                CommonAlerts.loadFragment(context,AddNewCardFragment())
            }
        }
    }
}