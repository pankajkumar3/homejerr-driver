package com.homejerrrider.views.fragments.orderAccepted

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.homejerrrider.R
import com.homejerrrider.databinding.FragmentOrderAcceptedBinding
import com.homejerrrider.databinding.FragmentTrackBinding
import com.homejerrrider.views.fragments.track.TrackVM


class OrderAcceptedFragment : Fragment() {


    lateinit var vm: OrderAcceptedVM
    lateinit var binding: FragmentOrderAcceptedBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentOrderAcceptedBinding.inflate(inflater)
        return binding.root
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onbind()
    }
    private fun onbind() {
        vm = OrderAcceptedVM(context!!)
        binding.viewModel = vm
    }
}