package com.homejerrrider.views.fragments.track

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.homejerrrider.R
import com.homejerrrider.databinding.FragmentTrackBinding
import com.homejerrrider.databinding.FragmentWalletBinding
import com.homejerrrider.views.fragments.walletFragment.WalletVM

class TrackFragment : Fragment() {
    lateinit var vm: TrackVM
    lateinit var binding: FragmentTrackBinding
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTrackBinding.inflate(inflater)
        return binding.root
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onbind()
    }
    private fun onbind() {
        vm = TrackVM(context!!)
        binding.viewModel = vm
    }
}