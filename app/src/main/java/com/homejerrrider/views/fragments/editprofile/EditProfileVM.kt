package com.homejerrrider.views.fragments.editprofile

import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.homejerrrider.MainActivity
import com.homejerrrider.networkCalls.RequestProcess
import com.homejerrrider.networkCalls.RetrofitApi
import com.homejerrrider.networkCalls.RetrofitCall
import com.homejerrrider.sharedpreference.PreferenceFile
import com.homejerrrider.utils.CommonMethods
import com.homejerrrider.utils.HomeInterface
import com.homejerrrider.utils.PermissionUtils.arePermissionsGranted
import com.homejerrrider.utils.getPartFromFile
import com.homejerrrider.utils.getPartRequestBody
import com.homejerrrider.validate.ValidatorUtils
import com.homejerrrider.views.addBankAccount.AddBankAccountActivity
import com.homejerrrider.views.login.signinresponse.SigninResponse
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response

class EditProfileVM(val context: Context, private val editProfile: EditProfile) : ViewModel() {

    var fullname = ObservableField("")
    var viewSelected = ObservableField(0)

    var username = ObservableField("")
    var email = ObservableField("")
    var street = ObservableField("")
    var address = ObservableField("")
    var pin = ObservableField("")
    var plate1 = ObservableField("")
    var plate2 = ObservableField("")
    var location = ObservableField("")
    var bank = ObservableField("")
    private var lat = ObservableField("30.7130")
    private var lng = ObservableField("76.7093")

    var phone = ObservableField("")
    var imagePath = ObservableField("")
    var imagePathlocal = ObservableField("")
    private var PERMISSION_CODE = 101
    var selectedCode = ObservableField(91)

    companion object {
        var homeInterface: HomeInterface? = null
    }

    val gson = Gson()

    val userData = PreferenceFile.retrieveLoginData(context)


    fun setUserData() {
        fullname.set(userData?.data?.fullName)
        email.set(userData?.data?.email)
        phone.set(userData?.data?.phone)
        street.set(userData?.data?.streetName)
        pin.set(userData?.data?.pincode)
        //  pin.set(data.)
        location.set(userData?.data?.address)
        plate1.set(userData?.data?.vehiclePlate1)
        plate2.set(userData?.data?.vehiclePlate2)
        username.set(userData?.data?.userName)
        imagePath.set(userData?.data?.image)
    }


    fun clicks(value: String) {
        when (value) {
            "drawer" -> {
                homeInterface!!.backPress()
            }
            "image" -> {
                selectImage()
            }
            "addCard" -> {
                (context as MainActivity).startActivity(
                    Intent(context, AddBankAccountActivity::class.java
                    ).putExtra("comes", "edit")
                )
            }
            "save" -> {
                callEditprofile()
            }
        }
    }

    private fun validation(): Boolean {
        when {
//            imagePath.get()!!.trim() == "" -> {
//                CommonMethods.showToast(context, "please upload your profile pic")
//                return false
//            }
            username.get()!!.trim() == "" -> {
                CommonMethods.showToast(context, "please enter user name")
                return false
            }
            !ValidatorUtils.isNameValid(username.get()!!.trim()) -> {
                CommonMethods.showToast(context, "Please enter valid user name")
                return false
            }

            phone.get()!!.trim() == "" -> {
                CommonMethods.showToast(context, "please enter your phone number")
                return false
            }
            !ValidatorUtils.isMobileValid(phone.get()!!.trim()) -> {
                CommonMethods.showToast(context, "please enter valid phone number")
                return false
            }
            /*  bank.get()!!.trim() == "" -> {
                  CommonMethods.showToast(context, "please enter your bank name")
                  return false
              }*/
        }
        return true
    }

    private fun callEditprofile() {
        val map = HashMap<String, RequestBody>()
        map["userName"] = getPartRequestBody(username.get()!!)
        map["mobileNo"] = getPartRequestBody(phone.get()!!)
        map["fullName"] = getPartRequestBody(fullname.get()!!)
        map["streetName"] = getPartRequestBody(street.get()!!)
        map["address"] = getPartRequestBody(location.get()!!)
        map["mobileNo"] = getPartRequestBody("123456789")
        map["bankName"] = getPartRequestBody("State Bank Of India")
        map["pincode"] = getPartRequestBody(pin.get()!!)
        map["lat"] = getPartRequestBody(lat.get()!!)
        map["lng"] = getPartRequestBody(lng.get()!!)

        var part: MultipartBody.Part? = null
        if (!imagePathlocal.get().isNullOrEmpty())
            part = getPartFromFile(imagePathlocal.get()!!, "image")

        val token = userData!!.data.accessToken

        RetrofitCall().callService(
            context,
            true,
            token,
            object : RequestProcess<Response<SigninResponse>> {
                override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                    Log.v("TAG", "request")
                    return retrofitApi.profileEdit(map, part)
                }

                override fun onResponse(response: Response<SigninResponse>) {

                    if (response.isSuccessful && response.body()!!.success) {
                        PreferenceFile.storeLoginData(context, response.body()!!)
                        // PreferenceFile.storeLoginData(context, response.body()!!)

                        Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT)
                            .show()
                        homeInterface!!.backPress()

                    } else {
                        CommonMethods.showToast(context, response.body()!!.message)
                    }
                }

                override fun onException(message: String?) {
                    Log.e("verifyException", "====$message")
                }
            })
    }

    private fun selectImage() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (context.arePermissionsGranted(PERMISSION_CODE)) {
                openImagePicker()
            }
        } else {
            openImagePicker()
        }
    }

    private fun openImagePicker() {
        CropImage.activity()
            .setGuidelines(CropImageView.Guidelines.ON)
            .setFixAspectRatio(true)
            .start(context, editProfile)
    }
}