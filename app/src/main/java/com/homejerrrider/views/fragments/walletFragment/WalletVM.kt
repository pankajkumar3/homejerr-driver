package com.homejerrrider.views.fragments.walletFragment

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModel
import com.homejerrrider.R
import com.homejerrrider.utils.CommonAlerts
import com.homejerrrider.utils.HomeInterface
import com.homejerrrider.views.fragments.paymentFragment.PaymentFragment

class WalletVM(val context: Context):ViewModel(){
    companion object{
        var homeInterface:HomeInterface?=null
    }
    fun clicks(value:String)
    {
        when(value)
        {
            "top"->
            {
                topBox()
            }
            "next"->
            {
                CommonAlerts.loadFragment(context,PaymentFragment())
            }
            "back"->
            {
                homeInterface!!.backPress()
            }
        }
    }
    private fun topBox()
    {
        val mDialogView =
                LayoutInflater.from(context).inflate(R.layout.topup_dialog, null)
        val mBuilder = AlertDialog.Builder(context).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCanceledOnTouchOutside(true)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val tvSubmitChange: TextView = mDialogView.findViewById<View>(R.id.btnNext) as TextView
        val edtAmount:EditText=mDialogView.findViewById<View>(R.id.etAmt) as EditText
        val ivCross: ImageView = mDialogView.findViewById<View>(R.id.ivCross) as ImageView
        ivCross.setOnClickListener {
            mAlertDialog.dismiss()
        }
        tvSubmitChange.setOnClickListener {
            CommonAlerts.loadFragment(context,PaymentFragment())
            mAlertDialog.dismiss()
        }
    }
}