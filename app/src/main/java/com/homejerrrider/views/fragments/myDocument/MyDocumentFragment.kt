package com.homejerrrider.views.fragments.myDocument

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.homejerrrider.databinding.FragmentMyDocumentBinding


class MyDocumentFragment : Fragment() {
private var vm:MyDocumentVM?=null
    var binding: FragmentMyDocumentBinding?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentMyDocumentBinding.inflate(inflater)
        return binding!!.root


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onBind()
    }



    private fun onBind() {
        vm = MyDocumentVM(context!!)
        binding!!.vm = vm
    }

    fun onBackpress() {
        super.onResume()
        onBackpress()
    }


}