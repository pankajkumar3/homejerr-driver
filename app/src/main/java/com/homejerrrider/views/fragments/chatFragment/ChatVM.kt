package com.homejerrrider.views.fragments.chatFragment

import android.content.Context
import androidx.lifecycle.ViewModel
import com.homejerrrider.utils.HomeInterface
import com.homejerrrider.views.fragments.chatFragment.adapter.ChatAdapter

class ChatVM(val context: Context):ViewModel() {
    companion object{
        var homeInterface:HomeInterface?=null
    }
    var chatAdapter=ChatAdapter(context)
    fun clicks(value:String)
    {
        when(value)
        {
            "back"->{
               homeInterface!!.backPress()
            }
        }
    }
}