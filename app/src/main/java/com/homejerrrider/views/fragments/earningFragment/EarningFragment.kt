package com.homejerrrider.views.fragments.earningFragment

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.homejerrrider.R
import com.homejerrrider.databinding.FragmentContactBinding
import com.homejerrrider.databinding.FragmentEarningBinding
import com.homejerrrider.views.fragments.contactFragment.ContactVM
import com.homejerrrider.views.fragments.earningFragment.adapter.AdapterBooking
import com.homejerrrider.views.fragments.earningFragment.adapter.AdapterEarningTabs
import kotlinx.android.synthetic.main.adapter_booking_single_row.*
import kotlinx.android.synthetic.main.fragment_earning.*

class EarningFragment : Fragment() {

    lateinit var vm: EarningVM
    lateinit var binding: FragmentEarningBinding
    var homeAdapter: AdapterEarningTabs? = null
    var reviewAdapter: AdapterBooking? = null
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? { binding = FragmentEarningBinding.inflate(inflater)
        return binding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onBind()
    }

    private fun onBind() {
        vm = EarningVM(context!!)
        binding.viewModel = vm
        recyclerEarn!!.layoutManager = LinearLayoutManager(activity)
        homeAdapter = AdapterEarningTabs(activity!!)
        btnEarn.setBackgroundResource(R.color.purple)
        btnEarn.setTextColor(Color.WHITE)
        recyclerEarn?.adapter = homeAdapter

        btnEarn.setOnClickListener {
            recyclerEarn!!.layoutManager = LinearLayoutManager(activity)
            homeAdapter = AdapterEarningTabs(activity!!)
            recyclerEarn?.adapter = homeAdapter

            btnEarn.setBackgroundResource(R.color.purple)
            btnEarn.setTextColor(Color.WHITE)
            recyclerBOOK.visibility = View.GONE
            recyclerEarn.visibility = View.VISIBLE
            btnBooking.setBackgroundResource(R.color.white)
            btnBooking.setTextColor(Color.BLACK)

            tvTxt1.visibility = View.VISIBLE
            txtPayment.visibility = View.VISIBLE
            txtEarn.visibility = View.VISIBLE
        }
        btnBooking.setOnClickListener {
            recyclerBOOK!!.layoutManager = LinearLayoutManager(activity)
            reviewAdapter = AdapterBooking(activity!!)
            recyclerBOOK?.adapter = reviewAdapter

            btnBooking.setBackgroundResource(R.color.purple)
            btnBooking.setTextColor(Color.WHITE)
            recyclerEarn.visibility = View.GONE
            recyclerBOOK.visibility = View.VISIBLE

            btnEarn.setBackgroundResource(R.color.white)

            btnEarn.setTextColor(Color.BLACK)

            tvTxt1.visibility = View.GONE
            txtPayment.visibility = View.GONE
            txtEarn.visibility = View.GONE
        }
    }
}