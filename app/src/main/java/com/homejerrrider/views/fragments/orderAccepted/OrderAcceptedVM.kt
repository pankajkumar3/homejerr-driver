package com.homejerrrider.views.fragments.orderAccepted

import android.content.Context
import android.os.Bundle
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.homejerrrider.utils.CommonAlerts
import com.homejerrrider.utils.HomeInterface
import com.homejerrrider.views.fragments.chatFragment.ChatFragment
import com.homejerrrider.views.fragments.orderIDFragment.OrderIDFragment
import com.homejerrrider.views.fragments.track.TrackFragment

class OrderAcceptedVM(val context: Context):ViewModel() {
    var type="orderAccepted"
    companion object{
        var homeInterface:HomeInterface?=null
    }
    var changeTxt = ObservableField("Order is Accepted!")
    var codeBooleanBtn=ObservableField(true)

    fun clicks(value:String)
    {
        when(value)
        {
            "pickUp"->
            {
                CommonAlerts.loadFragment(context,TrackFragment())
            }
            "chat"->
            {
                CommonAlerts.loadFragment(context,ChatFragment())
            }
            "orderID"->
            {
                CommonAlerts.loadFragment(context, OrderIDFragment())
            }
        }
    }
}