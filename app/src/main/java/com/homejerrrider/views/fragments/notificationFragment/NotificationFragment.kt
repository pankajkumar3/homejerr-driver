package com.homejerrrider.views.fragments.notificationFragment

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.homejerrrider.R
import com.homejerrrider.databinding.FragmentNotificationBinding
import kotlinx.android.synthetic.main.fragment_notification.*

class NotificationFragment : Fragment() {
    lateinit var vm: NotificationVM
    lateinit var binding:FragmentNotificationBinding
    var type=""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNotificationBinding.inflate(inflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onBind(type)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onBind(type)
        tabs()
    }
    private fun onBind(type:String) {
        vm = NotificationVM(context!!)
        binding.viewModel = vm
    }
    fun tabs()
    {
        noticeTab.setTextColor(Color.WHITE)
        noticeTab.setBackgroundResource(R.drawable.round_purple_border_tab)
        deliveryTab.setTextColor(Color.GRAY)
        deliveryTab.setBackgroundResource(R.drawable.round_grey_border_tab)
        paymentTab.setTextColor(Color.GRAY)
        paymentTab.setBackgroundResource(R.drawable.round_grey_border_tab)
        promoteTab.setTextColor(Color.GRAY)
        promoteTab.setBackgroundResource(R.drawable.round_grey_border_tab)

        noticeTab.setOnClickListener {
            noticeTab.setTextColor(Color.WHITE)
            noticeTab.setBackgroundResource(R.drawable.round_purple_border_tab)

            deliveryTab.setTextColor(Color.GRAY)
            deliveryTab.setBackgroundResource(R.drawable.round_grey_border_tab)
            paymentTab.setTextColor(Color.GRAY)
            paymentTab.setBackgroundResource(R.drawable.round_grey_border_tab)
            promoteTab.setTextColor(Color.GRAY)
            promoteTab.setBackgroundResource(R.drawable.round_grey_border_tab)

        }
        deliveryTab.setOnClickListener {
            deliveryTab.setTextColor(Color.WHITE)
            deliveryTab.setBackgroundResource(R.drawable.round_purple_border_tab)

            noticeTab.setTextColor(Color.GRAY)
            noticeTab.setBackgroundResource(R.drawable.round_grey_border_tab)
            paymentTab.setTextColor(Color.GRAY)
            paymentTab.setBackgroundResource(R.drawable.round_grey_border_tab)
            promoteTab.setTextColor(Color.GRAY)
            promoteTab.setBackgroundResource(R.drawable.round_grey_border_tab)

        }
        paymentTab.setOnClickListener {
            paymentTab.setTextColor(Color.WHITE)
            paymentTab.setBackgroundResource(R.drawable.round_purple_border_tab)

            deliveryTab.setTextColor(Color.GRAY)
            deliveryTab.setBackgroundResource(R.drawable.round_grey_border_tab)
            noticeTab.setTextColor(Color.GRAY)
            noticeTab.setBackgroundResource(R.drawable.round_grey_border_tab)
            promoteTab.setTextColor(Color.GRAY)
            promoteTab.setBackgroundResource(R.drawable.round_grey_border_tab)

        }
        promoteTab.setOnClickListener {
            promoteTab.setTextColor(Color.WHITE)
            promoteTab.setBackgroundResource(R.drawable.round_purple_border_tab)

            deliveryTab.setTextColor(Color.GRAY)
            deliveryTab.setBackgroundResource(R.drawable.round_grey_border_tab)
            noticeTab.setTextColor(Color.GRAY)
            noticeTab.setBackgroundResource(R.drawable.round_grey_border_tab)
            paymentTab.setTextColor(Color.GRAY)
            paymentTab.setBackgroundResource(R.drawable.round_grey_border_tab)
        }
    }

}