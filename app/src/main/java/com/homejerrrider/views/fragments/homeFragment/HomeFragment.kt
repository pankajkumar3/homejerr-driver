package com.homejerrrider.views.fragments.homeFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.homejerrrider.R
import com.homejerrrider.databinding.FragmentHomeBinding
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : Fragment() {
    private var vm: HomefragmentVM? = null
    var binding: FragmentHomeBinding? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater)
        return binding!!.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onbind()
    }

    private fun onbind() {
        vm = HomefragmentVM(context!!)
        binding!!.viewModel = vm

        binding!!.chkState.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                tvOffline.text = requireContext().getString(R.string.online)
                binding!!.clOffline.visibility = View.GONE
                binding!!.clBookings.visibility = View.VISIBLE
                binding!!.mainLayout.setBackgroundResource(R.drawable.online)
            } else {
                tvOffline.text = requireContext().getString(R.string.offline)
                binding!!.clOffline.visibility = View.VISIBLE
                binding!!.clBookings.visibility = View.GONE
                binding!!.mainLayout.setBackgroundResource(R.drawable.mask_group_1833)
            }
        }
    }
}


