package com.homejerrrider.views.fragments.editprofile.editresponse

data class BankAccount(
    val bankName: String,
    val mobileNo: String
)