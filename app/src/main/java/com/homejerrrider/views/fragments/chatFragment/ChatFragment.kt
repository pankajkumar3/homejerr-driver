package com.homejerrrider.views.fragments.chatFragment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.homejerrrider.databinding.FragmentChatBinding
import com.homejerrrider.utils.CommonAlerts


class ChatFragment : Fragment() {

    private var vm: ChatVM? = null
    var binding: FragmentChatBinding? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        CommonAlerts.hideKeyboard(context as Activity)


        binding = FragmentChatBinding.inflate(inflater)

        return binding!!.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onBind()
    }

    private fun onBind() {
        vm = ChatVM(context!!)
        binding!!.viewModel = vm
    }
}