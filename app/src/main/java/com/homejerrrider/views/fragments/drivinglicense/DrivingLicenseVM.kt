package com.homejerrrider.views.fragments.drivinglicense

import android.content.Context
import androidx.lifecycle.ViewModel
import com.homejerrrider.utils.HomeInterface

class DrivingLicenseVM(var context: Context) : ViewModel() {
    companion object {
        var homeInterface: HomeInterface? = null
    }

    fun onClicks(type: String) {
        when (type) {
            "back" -> {
                DrivingLicenseVM.homeInterface!!.backPress()
            }
            "saveFromDrivingLicense"->{
                DrivingLicenseVM.homeInterface!!.backPress()

            }
        }
    }
}