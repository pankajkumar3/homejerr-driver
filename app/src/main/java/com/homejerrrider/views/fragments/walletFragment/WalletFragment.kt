package com.homejerrrider.views.fragments.walletFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.homejerrrider.R
import com.homejerrrider.databinding.FragmentPaymentBinding
import com.homejerrrider.databinding.FragmentWalletBinding
import com.homejerrrider.views.fragments.paymentFragment.PaymentVM


class WalletFragment : Fragment() {

    lateinit var vm: WalletVM
    lateinit var binding: FragmentWalletBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentWalletBinding.inflate(inflater)
        return binding.root
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onbind()
    }

    private fun onbind() {
        vm = WalletVM(context!!)
        binding.viewModel = vm
    }
}