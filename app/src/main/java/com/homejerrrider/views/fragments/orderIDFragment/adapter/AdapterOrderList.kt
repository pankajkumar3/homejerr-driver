package com.homejerrrider.views.fragments.orderIDFragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.homejerrrider.R
import kotlinx.android.synthetic.main.adapter_chat_layout.view.*
import kotlinx.android.synthetic.main.adapter_order.view.*

class AdapterOrderList(val context:Context):RecyclerView.Adapter<AdapterOrderList.MyViewHolder>() {
    class MyViewHolder(itemView:View):RecyclerView.ViewHolder(itemView) {
        var dishImage = itemView.imgDish
        var dishPrice = itemView.tvDollar
        var dishName = itemView.tvDishName
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layout1=LayoutInflater.from(context).inflate(R.layout.adapter_order,parent,false)
        return MyViewHolder(layout1)
    }

    override fun getItemCount(): Int {
       return 5
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
      if(position%2==0)
                {
                    holder.dishName.visibility = VISIBLE
                    holder.dishName.setText("Beef Soup")
                    holder.dishPrice.visibility = VISIBLE
                    holder.dishPrice.setText("$7.90")
                }

        else
            {
                holder.dishName.visibility=VISIBLE
                holder.dishName.setText("Vegetable Pasta")
                holder.dishPrice.visibility= VISIBLE
                holder.dishPrice.setText("$2.90")
                holder.dishImage.setImageResource(R.drawable.pasta)
            }
        }
    }
