package com.homejerrrider.views.fragments.processOrder

import android.content.Context
import android.os.Bundle
import androidx.lifecycle.ViewModel
import com.homejerrrider.utils.CommonAlerts
import com.homejerrrider.utils.HomeInterface
import com.homejerrrider.views.fragments.acceptOrder.AcceptedOrderFragment
import com.homejerrrider.views.fragments.homeFragment.HomeFragment
import com.homejerrrider.views.fragments.orderAccepted.OrderAcceptedFragment

class ProcessingOrderVM(val context: Context):ViewModel() {
    var type="Accepted"
    companion object{
        var homeInterface:HomeInterface?=null
    }
    fun click(value:String)
    {
        when(value)
        {
            "slideToCancel"->
            {
                CommonAlerts.loadFragment(context,HomeFragment())
            }
            "next"->
            {
                val fragment = OrderAcceptedFragment()
                val bundle = Bundle()
                bundle.putString("accepted",type)
                fragment.arguments = bundle
                CommonAlerts.loadFragment(context,fragment)
            }
        }
    }
}