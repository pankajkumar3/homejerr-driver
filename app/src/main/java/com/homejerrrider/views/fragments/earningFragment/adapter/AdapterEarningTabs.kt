package com.homejerrrider.views.fragments.earningFragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.homejerrrider.R
import kotlinx.android.synthetic.main.adapter_earning_row.view.*
import kotlinx.android.synthetic.main.fragment_earning.view.*

class AdapterEarningTabs(val context: Context):RecyclerView.Adapter<AdapterEarningTabs.MyViewModel>() {
    class MyViewModel (itemView: View) : RecyclerView.ViewHolder(itemView) {
        var layoutEarning = itemView.layoutEarning
        var btnearning = itemView.btnEarn
        var btnbooking = itemView.btnBooking
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewModel{
       val layout=LayoutInflater.from(context).inflate(R.layout.adapter_earning_row,parent,false)
        return MyViewModel(layout)
    }

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(holder: AdapterEarningTabs.MyViewModel, position: Int) {

    }
}