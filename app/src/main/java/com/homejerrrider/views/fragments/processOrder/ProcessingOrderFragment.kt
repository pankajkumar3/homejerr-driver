package com.homejerrrider.views.fragments.processOrder

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.homejerrrider.R
import com.homejerrrider.databinding.FragmentAcceptedOrderBinding
import com.homejerrrider.databinding.FragmentProcessingOrderBinding
import com.homejerrrider.views.fragments.acceptOrder.AcceptedOrderVM

class ProcessingOrderFragment : Fragment() {

    lateinit var vm: ProcessingOrderVM
    lateinit var binding: FragmentProcessingOrderBinding
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding= FragmentProcessingOrderBinding.inflate(inflater)
        return binding.root
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onbind()
    }
    private fun onbind(){
        vm= ProcessingOrderVM(context!!)
        binding.viewModel=vm
    }
}