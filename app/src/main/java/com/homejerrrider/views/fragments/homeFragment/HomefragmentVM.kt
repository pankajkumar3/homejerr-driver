package com.homejerrrider.views.fragments.homeFragment

import android.content.Context
import android.os.Bundle
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.homejerrrider.utils.CommonAlerts
import com.homejerrrider.utils.HomeInterface
import com.homejerrrider.views.fragments.acceptOrder.AcceptedOrderFragment
import com.homejerrrider.views.fragments.notificationFragment.NotificationFragment
import com.homejerrrider.views.fragments.processOrder.ProcessingOrderFragment

class HomefragmentVM(val context: Context):ViewModel() {
    var boolean = ObservableBoolean(false)
    var booking="accept the order"
    companion object{
        var homeInterface: HomeInterface?=null
    }
    fun clicks(value:String) {
        when (value) {
            "drawer" -> {
                homeInterface!!.homeToggle()
            }
            "notification" -> {
                CommonAlerts.loadFragment(context,NotificationFragment())
            }
            "accept"->
            {
                CommonAlerts.loadFragment(context,ProcessingOrderFragment())
            }
            "next" ->
            {
                val fragment = AcceptedOrderFragment()
                val bundle = Bundle()
                bundle.putString("type",booking)
                fragment.arguments = bundle
                CommonAlerts.loadFragment(context,fragment)
            }
        }
    }
}
