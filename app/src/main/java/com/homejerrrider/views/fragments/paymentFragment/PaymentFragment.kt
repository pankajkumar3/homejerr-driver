package com.homejerrrider.views.fragments.paymentFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.homejerrrider.R
import com.homejerrrider.databinding.FragmentPaymentBinding
import com.homejerrrider.databinding.FragmentSupportBinding
import com.homejerrrider.views.fragments.supportFragment.SupportVM


class PaymentFragment : Fragment() {


    lateinit var vm: PaymentVM
    lateinit var binding: FragmentPaymentBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentPaymentBinding.inflate(inflater)
        return binding.root
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onbind()
    }
    private fun onbind(){
        vm= PaymentVM(context!!)
        binding.viewModel=vm
    }
}