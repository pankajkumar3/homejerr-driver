package com.homejerrrider.views.fragments.editprofile.editresponse

data class DriverDocument(
    val _id: String,
    val docImageBack: String,
    val docImageFront: String,
    val name: String
)