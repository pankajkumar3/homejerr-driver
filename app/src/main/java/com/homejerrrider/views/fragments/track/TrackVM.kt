package com.homejerrrider.views.fragments.track

import android.content.Context
import android.graphics.Color
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.homejerrrider.R
import com.homejerrrider.utils.CommonAlerts
import com.homejerrrider.utils.HomeInterface
import com.homejerrrider.views.fragments.chatFragment.ChatFragment
import com.homejerrrider.views.fragments.completeOrder.CompleteOrderFragment

class TrackVM(val context: Context):ViewModel() {
    val chgHeading=ObservableField("Pickup fast to get more orders")
    val codeBooleanslide=ObservableField(true)
    val codeBooleanslide5=ObservableField(true)
    val codeBooleanslide2=ObservableField(true)
    val codeBooleanslide3=ObservableField(true)
    val codeBooleanslide4=ObservableField(true)
    val chgOrderDeliver=ObservableField("This order has been delivered")
    val chgOnWay=ObservableField("On the way")
    val chgStore=ObservableField("At Store")
    val codeBool=ObservableField(true)
    val storeColor=ObservableField(Color.GRAY)
    val wayColor=ObservableField(Color.GRAY)
    val deliverColor=ObservableField(Color.GRAY)
    val codeUserVisible=ObservableField(true)
    val codeMarker=ObservableField(false)
    val codeMarkerPurple=ObservableField(true)
    val codeMarkerPurple1=ObservableField(true)
    val codeMarkerPurple2=ObservableField(true)
    val codeRoad=ObservableField(true)
    val chgText=ObservableField("Pickup")
//    val img=ObservableField(R.drawable.ic_greymarker)

    companion object{
        var homeInterface:HomeInterface?=null
    }
    fun clicks(value:String)
    {
        when (value) {
            "back" -> {
                homeInterface!!.backPress()
            }
            "slide1"->
            {
                codeBooleanslide.set(false)
                codeBooleanslide2.set(false)
                storeColor.set(Color.BLACK)
                codeMarkerPurple.set(false)
                chgHeading.set("Pickup this order")
            }
            "slide2"->
            {
                codeBooleanslide.set(false)
                codeBooleanslide2.set(true)
                codeMarkerPurple.set(false)
                codeMarkerPurple1.set(false)
                codeRoad.set(false)
                chgText.set("Delivery")
                codeBooleanslide3.set(false)
                storeColor.set(Color.BLACK)
                wayColor.set(Color.BLACK)
                chgHeading.set("Deliver fast to get more orders")
            }

            "slide3"->
            {
                codeBooleanslide.set(false)
                codeBooleanslide3.set(true)
                codeBooleanslide4.set(false)
                codeMarkerPurple.set(false)
                codeMarkerPurple1.set(false)
                codeRoad.set(false)
                chgText.set("Delivery")
                storeColor.set(Color.BLACK)
                wayColor.set(Color.BLACK)
                chgHeading.set("Deliver fast to get more orders")
            }

            "slide4"->
            {
                codeBooleanslide.set(false)
                codeBooleanslide4.set(false)
                codeBooleanslide5.set(false)
                codeBool.set(false)
                codeMarkerPurple.set(false)
                codeMarkerPurple1.set(false)
                codeRoad.set(false)
                chgText.set("Destination")
                codeUserVisible.set(false)
                wayColor.set(Color.BLACK)
                chgHeading.set("Complete your ride")
                chgStore.set("Arrived at stoppage")
                chgOrderDeliver.set("Ride is completed")
            }
            "slide5"->
            {
                CommonAlerts.loadFragment(context,CompleteOrderFragment())
            }
            "chat"->
            {
                CommonAlerts.loadFragment(context,ChatFragment())
            }
        }
    }
}