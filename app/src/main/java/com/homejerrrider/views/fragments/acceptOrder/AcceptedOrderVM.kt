package com.homejerrrider.views.fragments.acceptOrder

import android.content.Context
import android.os.Bundle
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.homejerrrider.utils.CommonAlerts
import com.homejerrrider.utils.HomeInterface
import com.homejerrrider.views.fragments.chatFragment.ChatFragment
import com.homejerrrider.views.fragments.contactFragment.ContactFragment
import com.homejerrrider.views.fragments.orderAccepted.OrderAcceptedFragment
import com.homejerrrider.views.fragments.orderIDFragment.OrderIDFragment
import com.homejerrrider.views.fragments.processOrder.ProcessingOrderFragment
import com.homejerrrider.views.fragments.track.TrackFragment

class AcceptedOrderVM(val context: Context,val bundle: Bundle):ViewModel() {
    var changeTxt = ObservableField("Accept the order!")
    var codeBooleanBtn=ObservableField(true)

    var type="acceptOrder"
    companion object{
        var homeInterface:HomeInterface?=null
    }
//    init
//    {
//        if(type=="accepted")
//        {
//            changeTxt.set("Order is Accepted!")
//            codeBooleanBtn.set(false)
//        }
//    }
    fun clicks(value:String)
    {
        when(value)
        {
            "back"->
            {
                homeInterface!!.backPress()
            }
            "orderID"->
            {
                val fragment = OrderIDFragment()
                val bundle = Bundle()
                bundle.putString("acceptTheOrder",type)
                fragment.arguments = bundle
                CommonAlerts.loadFragment(context,fragment)
            }
            "accept"->
            {
                CommonAlerts.loadFragment(context,ProcessingOrderFragment())
            }
            "chat"->
            {
                CommonAlerts.loadFragment(context,ChatFragment())
            }
            "contact"->
            {
                CommonAlerts.loadFragment(context,ContactFragment())
            }
        }
    }
}