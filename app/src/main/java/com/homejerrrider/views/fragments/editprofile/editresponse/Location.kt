package com.homejerrrider.views.fragments.editprofile.editresponse

data class Location(
    val coordinates: List<Double>,
    val type: String
)