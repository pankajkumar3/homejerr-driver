package com.homejerrrider.views.fragments.editprofile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.homejerrrider.MainActivity
import com.homejerrrider.databinding.EditprofileBinding
import com.theartofdev.edmodo.cropper.CropImage

class EditProfile : Fragment() {
    private var vm: EditProfileVM? = null
    var binding: EditprofileBinding? = null
    var transportMethods = arrayOf(
        "Food Truck/Grocery Delivery",
        "Grocery Delivery"
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = EditprofileBinding.inflate(inflater)
        vm= EditProfileVM(context!!, this)
        binding!!.vm = vm
        vm?.setUserData()
        return binding!!.root

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getData()
    }
    private fun getData() {
        if (arguments!=null){
            vm!!.bank.set(arguments!!.getString("bank").toString())
            vm!!.phone.set(arguments!!.getString("phone").toString())
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                val result = CropImage.getActivityResult(data)
                if (resultCode == Activity.RESULT_OK) {
                    val mUri = result.uri
                    if (vm!!.viewSelected.get() == 0) {
                        binding?.civProfile!!.setImageURI(mUri)
                        vm!!.imagePathlocal.set(mUri.path)
                    }
                }
            }
        }
    }
}