package com.homejerrrider.views.fragments.contactFragment

import android.app.Activity
import android.content.Context
import androidx.lifecycle.ViewModel
import com.homejerrrider.utils.CommonAlerts
import com.homejerrrider.utils.HomeInterface

class ContactVM(val context: Context):ViewModel() {
    companion object{
        var homeInterface: HomeInterface?=null
    }
    fun clicks(value:String) {
        when (value) {
            "drawer" -> {
                homeInterface!!.homeToggle()
            }
            "contact" -> {
                homeInterface!!.backPress()
                CommonAlerts.hideKeyboard(context as Activity)
            }
            "submit"->{
                homeInterface!!.backPress()

            }
        }
    }
}