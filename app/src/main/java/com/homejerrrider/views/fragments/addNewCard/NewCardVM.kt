package com.homejerrrider.views.fragments.addNewCard

import android.app.Activity
import android.content.Context
import androidx.lifecycle.ViewModel
import com.homejerrrider.utils.CommonAlerts
import com.homejerrrider.utils.HomeInterface

class NewCardVM(val context: Context):ViewModel() {
    companion object{
        var homeInterface:HomeInterface?=null
    }
    fun clicks(value:String)
    {
        when(value)
        {
            "back"->
            {
                homeInterface!!.backPress()
            }
            "next"->
            {
                homeInterface!!.backPress()
                CommonAlerts.hideKeyboard(context as Activity)
            }
            "cross"->{
                homeInterface!!.backPress()
            }
        }
    }
}