package com.homejerrrider.views.fragments.contactFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.homejerrrider.databinding.FragmentContactBinding


class ContactFragment : Fragment() {
    lateinit var vm: ContactVM
    lateinit var binding: FragmentContactBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentContactBinding.inflate(inflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onBind()
    }

    private fun onBind() {
        vm = ContactVM(context!!)
        binding.viewModel = vm
    }
}