package com.homejerrrider.views.fragments.completeOrder

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.homejerrrider.R
import com.homejerrrider.databinding.FragmentAcceptedOrderBinding
import com.homejerrrider.databinding.FragmentCompleteOrderBinding
import com.homejerrrider.views.fragments.acceptOrder.AcceptedOrderVM


class CompleteOrderFragment : Fragment() {

    lateinit var vm: CompleteOrderVM
    lateinit var binding: FragmentCompleteOrderBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentCompleteOrderBinding.inflate(inflater)
        return binding.root
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onbind()
    }

    private fun onbind(){
        vm=CompleteOrderVM(context!!)
        binding.viewModel=vm
    }
}