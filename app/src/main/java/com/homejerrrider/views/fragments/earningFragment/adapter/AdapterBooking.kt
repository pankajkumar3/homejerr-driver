package com.homejerrrider.views.fragments.earningFragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.homejerrrider.R
import com.homejerrrider.utils.CommonAlerts
import com.homejerrrider.views.fragments.completeOrder.CompleteOrderFragment
import kotlinx.android.synthetic.main.adapter_booking_single_row.view.*
import kotlinx.android.synthetic.main.fragment_earning.view.*

class AdapterBooking (val context: Context):RecyclerView.Adapter<AdapterBooking.MyViewModel>() {
    class MyViewModel (itemView: View) : RecyclerView.ViewHolder(itemView) {
        var layoutBooking = itemView.layoutBookings
        var btnearning = itemView.btnEarn
        var btnbooking = itemView.btnBooking
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewModel {
        val layout= LayoutInflater.from(context).inflate(R.layout.adapter_booking_single_row,parent,false)
        return MyViewModel(layout)
    }

    override fun getItemCount(): Int {
        return 6
    }

    override fun onBindViewHolder(holder: AdapterBooking.MyViewModel, position: Int) {
        holder.layoutBooking.setOnClickListener {
    CommonAlerts.loadFragment(context,CompleteOrderFragment())
}
    }
}