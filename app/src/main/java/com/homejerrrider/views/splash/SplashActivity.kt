package com.homejerrrider.views.splash

import android.app.Activity
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.facebook.login.Login
import com.homejerrrider.MainActivity
import com.homejerrrider.R
import com.homejerrrider.sharedpreference.PreferenceFile
import com.homejerrrider.sharedpreference.PreferenceKeys
import com.homejerrrider.views.login.SigninActivity
import com.homejerrrider.views.walkThrough.WalkThroughActivity

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.statusBarColor = resources.getColor(R.color.purple, this.theme)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = resources.getColor(R.color.purple)
        }
        setContentView(R.layout.activity_splash)
//
//        Handler(Looper.getMainLooper()).postDelayed({
//            if (PreferenceFile.retrieveKey(this, PreferenceKeys.loginData).isNullOrEmpty()) {
//                startActivity(Intent(this, SigninActivity::class.java))
//                finish()
//            } else if (PreferenceFile.retrieveKey(this, PreferenceKeys.loginData) == "") {
//                startActivity(Intent(this, WalkThroughActivity::class.java))
//                finish()
//            } else {
//                startActivity(Intent(this, MainActivity::class.java))
//                finish()
//            }
//
//        }, 3000)

         if (PreferenceFile.retrieveAuthToken(this)!=null)
        {
            Handler(Looper.getMainLooper()).postDelayed({
                val intent= Intent(this,MainActivity::class.java)
                startActivity(intent)
                finish()



            },2000)

        }
         else {
             Handler(Looper.getMainLooper()).postDelayed({
                 val intent = Intent(this, WalkThroughActivity::class.java)
                 startActivity(intent)
                 finish()


             }, 2000)
         }
    }
    }

