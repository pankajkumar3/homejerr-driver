package com.homejerrrider.views.documentic

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.homejerrrider.R
import com.homejerrrider.databinding.DocumentIcBinding
import com.homejerrrider.databinding.DrivingLicenseBinding
import com.homejerrrider.views.fragments.drivinglicense.DrivingLicenseVM

class DocumentIC : Fragment() {


     private var vm: DocumentICVM?=null
    var binding:DocumentIcBinding?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DocumentIcBinding.inflate(inflater)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBind()
    }

    private fun getBind(){
        vm= DocumentICVM(context!!)
        binding!!.vm=vm
    }

}