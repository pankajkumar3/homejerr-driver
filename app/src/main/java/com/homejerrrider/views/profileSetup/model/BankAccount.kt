package com.homejerrrider.views.profileSetup.model

data class BankAccount(
    var bankName: String,
    var mobileNo: String
)