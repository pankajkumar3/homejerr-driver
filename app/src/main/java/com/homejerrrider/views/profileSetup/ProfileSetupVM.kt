package com.homejerrrider.views.profileSetup

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.homejerrrider.R
import com.homejerrrider.networkCalls.RequestProcess
import com.homejerrrider.networkCalls.RetrofitApi
import com.homejerrrider.networkCalls.RetrofitCall
import com.homejerrrider.sharedpreference.PreferenceFile
import com.homejerrrider.sharedpreference.PreferenceKeys
import com.homejerrrider.utils.CommonAlerts
import com.homejerrrider.utils.CommonMethods
import com.homejerrrider.utils.CommonMethods.showToast
import com.homejerrrider.utils.PermissionUtils.arePermissionsGranted
import com.homejerrrider.utils.getPartFromFile
import com.homejerrrider.utils.getPartRequestBody
import com.homejerrrider.validate.ValidatorUtils
import com.homejerrrider.views.addBankAccount.AddBankAccountActivity
import com.homejerrrider.views.login.SigninActivity
import com.homejerrrider.views.login.signinresponse.SigninResponse
import com.homejerrrider.views.profileSetup.model.Data
import com.homejerrrider.views.upLoadDocument.DocumentUploadActivity
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response

class ProfileSetupVM(val context: Context) : ViewModel() {

    private var PERMISSION_CODE = 101
    var bankName = ObservableField("")
    var accNumber = ObservableField<String>("")
    var token = ObservableField<String>("")

    var imagePath = ObservableField("")
    var fullname = ObservableField("")
    var username = ObservableField("")
    var streetname = ObservableField("")
    var location = ObservableField("")
    var zipcode = ObservableField("")
    var userImage = ObservableField("")
    var isBankClicked = ObservableBoolean(false)
    var Vplate1 = ObservableField("")
    var Vplate2 = ObservableField("")
    var bank = ObservableField("")
    val codeBoolean = ObservableField(true)
    var lat = "0.0"
    var phone = ""
    var lng = "0.0"
    val gson = Gson()
    val new = PreferenceFile.retrieveKey(context, PreferenceKeys.loginData)
    val data: Data by lazy { Gson().fromJson(new, Data::class.java) }


    init {
        phone = PreferenceFile.retrieveKey(context, PreferenceKeys.phone).toString()
    }

    private fun selectImage() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (context.arePermissionsGranted(PERMISSION_CODE)) {
                openImagePicker()
            }
        } else {
            openImagePicker()
        }
    }

    private fun openImagePicker() {

        CropImage.activity()
            .setGuidelines(CropImageView.Guidelines.ON)
            .setFixAspectRatio(true)
            .start(context as ProfileSetupActivity)
    }

    fun onClicks(value: String) {
        when (value) {
            "back" -> {
                (context as ProfileSetupActivity).startActivity(
                    Intent(context,
                        SigninActivity::class.java
                    )
                )
                // (context as ProfileSetupActivity).onBackPressed()
                context.finish()
            }

            "bank" -> {

                val intent = Intent(context, AddBankAccountActivity::class.java)
                (context as ProfileSetupActivity).startActivityForResult(intent, 123)
            }
            "image" -> {
                selectImage()

            }

 /*           "nextAddBank" -> {
                if (validProfile()) {
                    CommonAlerts.hideKeyboard(context as Activity)
                    callSetProfile()
                }
            }*/

            "next" -> {

                if (validProfile()){
                    CommonAlerts.hideKeyboard(context as Activity)
                    callSetProfile()
                }
            }

        }
    }

    private fun validProfile(): Boolean {
        when {
            /* imagePath.get()!!.trim() == "" -> {
                 CommonMethods.showToast(context, context.getString(R.string.pleaseUploadYourName))
                 return false
             }*/

            fullname.get()!!.trim().isEmpty() -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterFullName))
                return false
            }

            !ValidatorUtils.isNameValid(fullname.get()!!.isEmpty().toString()) -> { CommonMethods.showToast(context, context.getString(R.string.pleaseEnterValidFullName))
                return false
            }
            username.get()!!.trim() == "" -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterUserName))
                return false
            }
            /* !ValidatorUtils.isNameValid(username.get()!!.trim()) -> {
                 CommonMethods.showToast(context, context.getString(R.string.pleaseEnterValidLName))
                 return false
             }*/
            streetname.get()!!.trim() == "" -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterStreetName))
                return false
            }
            zipcode.get()!!.trim() == "" -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterYourZipCode))
                return false
            }
            location.get()!!.trim() == "" -> {
                CommonMethods.showToast(
                    context,
                    context.getString(R.string.pleaseEnterYourLocation)
                )
                return false
            }
            /* bank.get()!!.trim() == "" -> {
                 CommonMethods.showToast(context, context.getString(R.string.pleaseEnterYourBankAccount))
                 return false
             }*/
            Vplate1.get()!!.trim() == "" -> {
                CommonMethods.showToast(
                    context,
                    context.getString(R.string.pleaseEnterVehiclePlate)
                )
                return false
            }
/*
            Vplate2.get()!!.trim() == "" -> {
                CommonMethods.showToast(
                    context,
                    context.getString(R.string.pleaseEnterYourVehiclePlate2)
                )
                return false
            }
*/
        }
        return true
    }

    private fun callSetProfile() {
        val map = HashMap<String, RequestBody>()
        map["fullName"] = getPartRequestBody(fullname.get()!!)
        map["userName"] = getPartRequestBody(username.get()!!)
        map["streetName"] = getPartRequestBody(streetname.get()!!)
        map["address"] = getPartRequestBody(location.get()!!)
        map["pincode"] = getPartRequestBody(zipcode.get()!!)
        map["mobileNo"] = getPartRequestBody(phone)
        map["bankName"] = getPartRequestBody(bankName.get()!!)
        map["vehiclePlate1"] = getPartRequestBody(Vplate1.get()!!)
        map["vehiclePlate2"] = getPartRequestBody(Vplate2.get()!!)
        map["lng"] = getPartRequestBody(lng)
        map["lat"] = getPartRequestBody(lat)

        var part: MultipartBody.Part? = null
        if (imagePath.get()!!.isNotEmpty()) {
            part = getPartFromFile(imagePath.get()!!, "image")
        }

        if (token.get()!!.isEmpty()) {
            token.set(PreferenceFile.retrieveAuthToken(context))
        }
        RetrofitCall().callService(
            context,
            true,
            token.get()!!,
            object : RequestProcess<Response<SigninResponse>> {
                override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                    return retrofitApi.profileSetup(map, part)
                }

                override fun onResponse(res: Response<SigninResponse>) {
                    val response = res.body()!!

                    val data: String = gson.toJson(res.body()!!.data)

                    if (res.isSuccessful && response.success) {

                       // PreferenceFile.storeLoginData(context, response)

                        // PreferenceFile.storeKey(context, PreferenceKeys.loginData, data)
                        //  PreferenceFile.storeKey(context, PreferenceKeys.token, res.body()!!.data.accessToken)
                        (context as ProfileSetupActivity)
                            .startActivity(Intent(context, DocumentUploadActivity::class.java).putExtra("token", token.get()))
                        context.finishAffinity()

                    }
                        showToast(context, res.body()!!.message)
                }

                override fun onException(message: String?) {
                    CommonMethods.showToast(context, "$message")
                }
            })
    }
}



