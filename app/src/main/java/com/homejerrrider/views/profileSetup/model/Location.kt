package com.homejerrrider.views.profileSetup.model

data class Location(
    var coordinates: List<Double>,
    var type: String
)