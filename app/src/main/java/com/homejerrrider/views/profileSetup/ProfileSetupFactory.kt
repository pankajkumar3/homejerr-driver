package com.homejerrrider.views.profileSetup

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ProfileSetupFactory(val context: Context): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ProfileSetupVM::class.java)) {
            return ProfileSetupVM(context) as T
        }
        throw IllegalArgumentException("")
    }
}