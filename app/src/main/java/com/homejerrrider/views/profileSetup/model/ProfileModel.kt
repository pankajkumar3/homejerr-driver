package com.homejerrrider.views.profileSetup.model

data class ProfileModel(
    var `data`: Data,
    var success: Boolean
)