package com.homejerrrider.views.profileSetup

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.homejerrrider.R
import com.homejerrrider.databinding.ActivityProfileSetupBinding
import com.theartofdev.edmodo.cropper.CropImage

class ProfileSetupActivity : AppCompatActivity() {
    private var binding: ActivityProfileSetupBinding? = null
    var vm: ProfileSetupVM? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile_setup)
        val factory = ProfileSetupFactory(this)
        vm = ViewModelProvider(this, factory).get(ProfileSetupVM::class.java)
        getData()
        binding?.vm = vm
    }

    private fun getData() {
        if (intent.hasExtra("token"))
        vm?.token?.set(intent.getStringExtra("token"))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                val result = CropImage.getActivityResult(data)
                if (resultCode == Activity.RESULT_OK) {
                    val mUri = result.uri
                    binding?.civProfile?.setImageURI(mUri)
                    vm!!.imagePath.set(mUri.path)
                }
            }

            123 -> {
                if (resultCode == RESULT_OK) {
                    vm!!.isBankClicked.set(true)
                    vm!!.accNumber.set(data?.getStringExtra("phone"))
                    vm!!.bankName.set(data?.getStringExtra("bank"))
                }
            }
        }
    }
}
