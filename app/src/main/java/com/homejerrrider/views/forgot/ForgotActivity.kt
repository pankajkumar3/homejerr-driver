package com.homejerrrider.views.forgot

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.homejerrrider.R
import com.homejerrrider.databinding.ActivityForgotBinding

class ForgotActivity : AppCompatActivity() {
    private var viewModel:ForgotVM?=null
     var binding: ActivityForgotBinding?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot)
        val factory = ForgotFactory(this,intent)
         viewModel = ViewModelProvider(this, factory).get(ForgotVM::class.java)
        binding?.viewModel=viewModel
    }
}