package com.homejerrrider.views.forgot.model

data class ForgotResponse(
    var message: String,
    var success: Boolean
)