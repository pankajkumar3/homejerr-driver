package com.homejerrrider.views.forgot

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.homejerrrider.networkCalls.RequestProcess
import com.homejerrrider.networkCalls.RetrofitApi
import com.homejerrrider.networkCalls.RetrofitCall
import com.homejerrrider.utils.CommonMethods.showToast
import com.homejerrrider.views.forgot.model.ForgotResponse
import com.homejerrrider.views.login.signinresponse.SigninResponse
import com.homejerrrider.views.verifyEmailPhone.verifyByEmail.EmailVerificationActivity
import com.homejerrrider.views.verifyEmailPhone.verifyByPhone.PhoneVerificationActivity
import retrofit2.Response

class ForgotVM(val context:Context,val intent: Intent):ViewModel() {
    val email = ObservableField("")
    var type = ""
    var emailOrPhone = ObservableField("")

    fun clicks(value: String) {
        when (value) {
            "back" -> {
                (context as Activity).onBackPressed()
            }
            "forgot" -> {
                type = if (TextUtils.isDigitsOnly(email.get())) {
                    Toast.makeText(context, "phone no", Toast.LENGTH_SHORT).show()
                    "phone"
                } else {
                    Toast.makeText(context, "email", Toast.LENGTH_SHORT).show()
                    "email"
                }

                callForgotPassword()

                //(context as ForgotPassword).startActivity(Intent(context, VerifyEmail::class.java).putExtra("class", "forgot"))
            }
        }
    }

    private fun callForgotPassword() {
        val jsonElement = JsonObject()
      //  jsonElement.addProperty("email", emailOrPhone.get())
        jsonElement.addProperty("email", email.get())

        try {
            RetrofitCall().callService(
                context,
                true,
                "",
                object : RequestProcess<retrofit2.Response<SigninResponse>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                        return retrofitApi.forgotPassword(jsonElement)
                    }

                    override fun onResponse(res: retrofit2.Response<SigninResponse>) {
                        val response = res.body()!!
                        if (res.isSuccessful) {
                            showToast(context,response.message)
                            if (response.success) {
                                if (email.get().toString().contains("@")) {
                                    (context as ForgotActivity).startActivity(Intent(context, EmailVerificationActivity::class.java).putExtra("comes","forgot").putExtra("data",email.get()!!))
                                    (context as ForgotActivity).finish()
                                } else{
                                    (context as ForgotActivity).startActivity(Intent(context, PhoneVerificationActivity::class.java).putExtra("comes","forgot").putExtra("data",email.get()))

                                    (context as ForgotActivity).finish()

                                }
                            }else {
                                showToast(context, response.message.toString())
                            }
                        } else {
                            showToast(context, response.message.toString())
                        }
                    }

                    override fun onException(message: String?) {
                        Log.e("verifyException", "====$message")
                    }
                })
        }
        catch (e: Exception)
        {
            e.printStackTrace()
        }
    }
}


