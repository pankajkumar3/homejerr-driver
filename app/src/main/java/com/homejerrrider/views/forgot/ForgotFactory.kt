package com.homejerrrider.views.forgot

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ForgotFactory (val context: Context, val intent: Intent): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ForgotVM::class.java)) {
            return ForgotVM(context, intent) as T
        }
        throw IllegalArgumentException("")
    }
}