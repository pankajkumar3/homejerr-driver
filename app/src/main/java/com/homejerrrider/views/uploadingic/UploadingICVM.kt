package com.homejerrrider.views.uploadingic

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.homejerrrider.networkCalls.RequestProcess
import com.homejerrrider.networkCalls.RetrofitApi
import com.homejerrrider.networkCalls.RetrofitCall
import com.homejerrrider.recyclerAdapter.ItemDocModel
import com.homejerrrider.sharedpreference.PreferenceFile
import com.homejerrrider.utils.CommonMethods
import com.homejerrrider.utils.PermissionUtils.arePermissionsGranted
import com.homejerrrider.utils.getPartFromFile
import com.homejerrrider.utils.getPartRequestBody
import com.homejerrrider.views.login.signinresponse.SigninResponse
import com.homejerrrider.views.upLoadDocument.uploaddocresponse.DocumentsResponse
import com.homejerrrider.views.welcomePage.WelcomePage
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response

class UploadingICVM(var context: Context, intent: Intent) : ViewModel() {

    var viewSelected = ObservableField(0)
    var front = ObservableField("")
    var back = ObservableField("")
    var name = ObservableField("ic")
    var imagePath = ObservableField("")
    var PERMISSION_CODE = 101
    var statusCode = ObservableField(3)
    var token = ObservableField("")
    var imageList = ArrayList<ItemDocModel>()


    fun onClicks(type: String) {
        when (type) {
            "backFromIC" -> {
                (context as UploadingIC).onBackPressed()
            }
            "icFrontImage" -> {
                viewSelected.set(0)
                selectImage()
            }
            "icBackImage" -> {
                viewSelected.set(1)
                selectImage()
            }
            "icUpload" -> {
                callUploadDocuments()
            }
        }
    }

    private fun selectImage() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (context.arePermissionsGranted(PERMISSION_CODE)) {
                openImagePicker()
            }
        } else {
            openImagePicker()
        }
    }

    private fun openImagePicker() {
        CropImage.activity()
            .setGuidelines(CropImageView.Guidelines.ON)
            .setFixAspectRatio(false)
            .start(context as UploadingIC)
    }


    private fun callUploadDocuments() {
        val map = HashMap<String, RequestBody>()
        var part1: MultipartBody.Part? = null
        var part: MultipartBody.Part? = null

        map["name"] = getPartRequestBody(name.get()!!)
        map["documentUploadStatus"] = getPartRequestBody(statusCode.get()!!.toString())
        if (front.get()!!.isNotEmpty()) part = getPartFromFile(front.get()!!, "front")
        if (back.get()!!.isNotEmpty()) part1 = getPartFromFile(back.get()!!, "back")
        if (token.get()!!.isEmpty()) {
            token.set(PreferenceFile.retrieveAuthToken(context))
        }
        RetrofitCall().callService(
            context,
            true,
            token.get()!!,
            object : RequestProcess<Response<SigninResponse>> {
                override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                    return retrofitApi.uploadDocs(map, part, part1)
                }

                override fun onResponse(response: Response<SigninResponse>) {
                    if (response.isSuccessful && response.body()!!.success) {
                        CommonMethods.showToast(context, response.body()!!.message)
                        PreferenceFile.storeAuthToken(context, response.body()!!.data.accessToken)
                        PreferenceFile.storeLoginData(context, response.body()!!)


                        (context as UploadingIC).startActivity(Intent(context, WelcomePage::class.java))
                        (context as UploadingIC).finishAffinity()
                    } else
                        CommonMethods.showToast(context, response.body()!!.message)
                }

                override fun onException(message: String?) {
                    Log.e("userException", "====$message")

                }


            })
    }


}