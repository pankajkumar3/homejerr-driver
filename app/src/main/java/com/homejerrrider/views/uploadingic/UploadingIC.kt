package com.homejerrrider.views.uploadingic

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.homejerrrider.R
import com.homejerrrider.databinding.UploadingIcBinding
import com.theartofdev.edmodo.cropper.CropImage

class UploadingIC : AppCompatActivity() {
    private var vm: UploadingICVM? = null
    var binding: UploadingIcBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.uploading_ic)
        binding = DataBindingUtil.setContentView(this, R.layout.uploading_ic)
        val factory = UploadingICFactory(this, intent)
        vm = ViewModelProvider(this, factory).get(UploadingICVM::class.java)
        getData()
        binding?.vm = vm
    }

    private fun getData() {
        if (intent.hasExtra("token"))
            vm?.token?.set(intent.getStringExtra("token"))

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                val result = CropImage.getActivityResult(data)
                if (resultCode == Activity.RESULT_OK) {
                    val mUri = result.uri
                    if (vm!!.viewSelected.get() == 0) {
                        binding?.ivICFirstFrontImage!!.setImageURI(mUri)
                        vm!!.front.set(mUri.path)
                        binding?.ivAddVehiclePlusFront?.isVisible = false
                        binding?.tvFrontIC?.isVisible = false
                        binding?.tvUploadingVehReg?.text = "IC Uploading.."
                        binding?.tvUploadMsg2?.text = "IC Uploading.."

                    } else {
                        binding?.ivUploadICBackImage!!.setImageURI(mUri)
                        vm!!.back.set(mUri.path)
                        binding?.ivAddICPlusBack?.isVisible = false
                        binding?.tvBackIC?.isVisible = false
                        binding?.tvUploadMsg2?.text = "IC Uploaded Successfully"
                        binding?.tvUploadingVehReg?.text = "IC Uploaded Successfully"

                    }
                }
            }
        }
    }

}