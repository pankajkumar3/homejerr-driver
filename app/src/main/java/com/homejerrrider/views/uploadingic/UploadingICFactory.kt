package com.homejerrrider.views.uploadingic

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class UploadingICFactory(val context: Context, val intent: Intent): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UploadingICVM::class.java)) {
            return UploadingICVM(context, intent) as T
        }
        throw IllegalArgumentException("")
    }
}