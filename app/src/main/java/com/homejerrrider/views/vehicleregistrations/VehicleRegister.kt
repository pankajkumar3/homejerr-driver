package com.homejerrrider.views.vehicleregistrations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.homejerrrider.R
import com.homejerrrider.databinding.DrivingLicenseBinding
import com.homejerrrider.databinding.VehicleRegisterBinding
import com.homejerrrider.views.fragments.drivinglicense.DrivingLicenseVM

class VehicleRegister : Fragment() {


    var vm: VehicleRegisterVM? = null
    var binding: VehicleRegisterBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = VehicleRegisterBinding.inflate(inflater)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBind()
    }
    private fun getBind(){
        vm= VehicleRegisterVM(context!!)
        binding!!.vm=vm
    }







}