package com.homejerrrider.views.vehicleregistrations

import android.content.Context
import androidx.lifecycle.ViewModel
import com.homejerrrider.utils.HomeInterface

class VehicleRegisterVM(var context: Context) : ViewModel() {
    companion object {
        var homeInterface: HomeInterface? = null
    }


    fun onClicks(type:String){
        when(type){

            "backFromVehicleRegister"->{
                VehicleRegisterVM.homeInterface!!.onBackPress()
            }
            "saveFromVehicleLicense"->{
                VehicleRegisterVM.homeInterface!!.onBackPress()


            }

        }

    }
}