package com.homejerrrider.views.upLoadDocument.uploaddocresponse

data class DocumentsResponse(
    val `data`: Data,
    val documentUploadStatus: Int,
    val message: String,
    val success: Boolean
)