package com.homejerrrider.views.upLoadDocument

import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.homejerrrider.networkCalls.RequestProcess
import com.homejerrrider.networkCalls.RetrofitApi
import com.homejerrrider.networkCalls.RetrofitCall
import com.homejerrrider.recyclerAdapter.ItemDocModel
import com.homejerrrider.sharedpreference.PreferenceFile
import com.homejerrrider.utils.CommonMethods
import com.homejerrrider.utils.PermissionUtils.arePermissionsGranted
import com.homejerrrider.utils.getPartFromFile
import com.homejerrrider.utils.getPartRequestBody
import com.homejerrrider.views.login.SigninActivity
import com.homejerrrider.views.login.signinresponse.SigninResponse
import com.homejerrrider.views.uploadvehicleregistration.UploadVehicleRegistration
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response

class DocumentUploadVM(val context: Context) : ViewModel() {

    var bankName = ObservableField("")
    var phone = ObservableField("")
    var viewSelected = ObservableField(0)

    var uploadedImageIs = ObservableField(0)
    var PERMISSION_CODE = 101
    var token = ObservableField("")
    var statusCode = ObservableField(1)
    var chgBtnName = ObservableField("")
    var type = " "
    var front = ObservableField("")
    var name = ObservableField("driving license")
    var back = ObservableField("")

    var imageList = ArrayList<ItemDocModel>()

    fun clicks(value: String) {
        when (value) {
            "frontImage" -> {
                viewSelected.set(0)
                selectImage()
            }
            "backImage" -> {
                viewSelected.set(1)
                selectImage()
            }
            "backFromDL" -> {
                (context as DocumentUploadActivity).startActivity(
                    Intent(context, SigninActivity::class.java)
                )
            }

            "upload" -> {
                if (CommonMethods.isNetworkAvailable(context)) {
                    callUploadDocuments()

                }
            }

        }
    }


    private fun selectImage() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (context.arePermissionsGranted(PERMISSION_CODE)) {
                openImagePicker()
            }
        } else {
            Toast.makeText(context, "Permission not granted", Toast.LENGTH_SHORT).show()
        }
    }

    private fun openImagePicker() {
        CropImage.activity()
            .setGuidelines(CropImageView.Guidelines.ON)
            .setFixAspectRatio(false)
            .start(context as DocumentUploadActivity)
    }

    private fun callUploadDocuments() {
        var part1: MultipartBody.Part? = null
        var part: MultipartBody.Part? = null

        val map = HashMap<String, RequestBody>()
        map["name"] = getPartRequestBody(name.get()!!)
        map["documentUploadStatus"] = getPartRequestBody(statusCode.get()!!.toString())
        if (front.get()!!.isNotEmpty()) part = getPartFromFile(front.get()!!, "front")
        if (back.get()!!.isNotEmpty()) part1 = getPartFromFile(back.get()!!, "back")


        if (token.get()!!.isEmpty()) {
            token.set(PreferenceFile.retrieveAuthToken(context))
        }
        RetrofitCall().callService(
            context,
            true,
            token.get()!!,
            object : RequestProcess<Response<SigninResponse>> {
                override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                    return retrofitApi.uploadDocs(map, part, part1)
                }

                override fun onResponse(response: Response<SigninResponse>) {
                    if (response.isSuccessful && response.body()!!.success) {
                        CommonMethods.showToast(context, response.body()!!.message)

                        (context as DocumentUploadActivity).startActivity(Intent(context, UploadVehicleRegistration::class.java).putExtra("token", token.get())
                        )
                        context.finishAffinity()
                    } else
                        CommonMethods.showToast(context, response.body()!!.message)
                }

                override fun onException(message: String?) {
                    Log.e("userException", "====$message")

                }


            })
    }
}
