package com.homejerrrider.views.upLoadDocument.uploaddocresponse

data class Location(
    val coordinates: List<Double>,
    val type: String
)