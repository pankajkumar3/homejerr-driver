package com.homejerrrider.views.upLoadDocument.uploaddocresponse

data class DriverDocument(
    val _id: String,
    val docImageBack: String,
    val docImageFront: String,
    val name: String
)