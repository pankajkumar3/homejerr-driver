package com.homejerrrider.views.upLoadDocument

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.homejerrrider.R
import com.homejerrrider.databinding.ActivityDocumentUploadBinding
import com.theartofdev.edmodo.cropper.CropImage

class DocumentUploadActivity : AppCompatActivity() {

    private var viewModel: DocumentUploadVM? = null
    var binding: ActivityDocumentUploadBinding? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_document_upload)
        val factory = DocumentUploadFactory(this)
        viewModel = ViewModelProvider(this, factory).get(DocumentUploadVM::class.java)
        getData()
        binding?.viewModel = viewModel

    }

    private fun getData() {
        viewModel?.token?.set(intent.getStringExtra("token"))

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                val result = CropImage.getActivityResult(data)
                if (resultCode == Activity.RESULT_OK) {
                    val mUri = result.uri
                    if (viewModel!!.viewSelected.get() == 0) {
                        binding?.ivFirstFrontImage!!.setImageURI(mUri)
                        viewModel!!.front.set(mUri.path)

                        binding?.tvFront?.isVisible = false
                        binding?.tvUploadMsg1?.text = "Driving License Uploading"
                        binding?.tvUploadMsg2?.text = "Driving License Uploading"
                        binding?.ivAddIconFront?.isVisible = false
                        viewModel?.uploadedImageIs?.set(1)

                    }
                    else
                    {
                        binding?.ivFirstBackImage!!.setImageURI(mUri)
                        viewModel!!.back.set(mUri.path)

                        viewModel?.uploadedImageIs?.set(2)

                        binding?.tvUploadMsg1?.text = "Driving License Uploaded Successfully"
                        binding?.tvUploadMsg2?.text = "Driving License Uploaded Successfully"
                        binding?.FirstBackVehicleRegBack?.isVisible=false
                        binding?.tvBack?.isVisible=false

                    }

                }
            }
        }
    }
}


