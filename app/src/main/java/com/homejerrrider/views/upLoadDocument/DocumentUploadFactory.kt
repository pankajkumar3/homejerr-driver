package com.homejerrrider.views.upLoadDocument

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class DocumentUploadFactory  (val context: Context): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DocumentUploadVM::class.java)) {
            return DocumentUploadVM(context) as T
        }
        throw IllegalArgumentException("")
    }
}
