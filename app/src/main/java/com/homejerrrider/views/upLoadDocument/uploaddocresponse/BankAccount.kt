package com.homejerrrider.views.upLoadDocument.uploaddocresponse

data class BankAccount(
    val bankName: String,
    val mobileNo: String
)