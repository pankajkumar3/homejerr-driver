package com.homejerrrider.views.login.signinresponse

data class SigninResponse(
    val `data`: Data,
    val isEmailVerified: Boolean,
    val message: String,
    val success: Boolean,
    val type: String
)