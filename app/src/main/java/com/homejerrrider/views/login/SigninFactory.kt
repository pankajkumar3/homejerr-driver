package com.homejerrrider.views.login

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SigninFactory (val context: Context, val intent: Intent): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SigninVM::class.java)) {
            return SigninVM(context, intent) as T
        }
        throw IllegalArgumentException("")
    }
}