package com.homejerrrider.views.login

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.facebook.CallbackManager
import com.github.angads25.toggle.interfaces.OnToggledListener
import com.google.gson.Gson
import com.homejerrrider.MainActivity
import com.homejerrrider.R
import com.homejerrrider.model.SaveMeData
import com.homejerrrider.networkCalls.RequestProcess
import com.homejerrrider.networkCalls.RetrofitApi
import com.homejerrrider.networkCalls.RetrofitCall
import com.homejerrrider.sharedpreference.PreferenceFile
import com.homejerrrider.utils.CommonMethods
import com.homejerrrider.views.forgot.ForgotActivity
import com.homejerrrider.views.login.signinresponse.SigninResponse
import com.homejerrrider.views.profileSetup.ProfileSetupActivity
import com.homejerrrider.views.signup.SignupActivity
import com.homejerrrider.views.upLoadDocument.DocumentUploadActivity
import com.homejerrrider.views.uploadingic.UploadingIC
import com.homejerrrider.views.uploadvehicleregistration.UploadVehicleRegistration
import com.homejerrrider.views.verifyEmailPhone.verifyByEmail.EmailVerificationActivity
import com.homejerrrider.views.verifyEmailPhone.verifyByPhone.PhoneVerificationActivity
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Response

class SigninVM(val context: Context, val intent: Intent) : ViewModel() {

    var ischecked = ObservableBoolean(false)
    var email = ObservableField("")
    var phone = ObservableField("")


    var password = ObservableField("")
    var rememberMe = ObservableBoolean(false)

    var type = ObservableField("")

    val gson = Gson()
    var callbackManager: CallbackManager? = null


    var toggleListener =
        OnToggledListener { _, isOn ->
            if (!isOn)
                PreferenceFile.clearForeverPreference(context)

            ischecked.set(isOn)

        }

    init {
        val saveMeData = PreferenceFile.retrieveRememberMe(context)
        if (saveMeData != null) {
            email.set(saveMeData.email)
            password.set(saveMeData.password)
            ischecked.set(saveMeData.isRemember)
        }
    }


    fun clicks(value: String) {
        when (value) {
            "forgot" -> {
                (context as SigninActivity).startActivity(
                    Intent(
                        context,
                        ForgotActivity::class.java
                    )
                )
            }
            "signUP" -> {
                (context as SigninActivity).startActivity(Intent(context, SignupActivity::class.java).putExtra("comes", "Login"))
            }
            "login" -> {


                when {
                    email.get()!!.isEmpty() -> { Toast.makeText(context, context.getString(R.string.please_enter_email_or_phone), Toast.LENGTH_SHORT).show()
                    }
                    password.get()!!.isEmpty() -> { Toast.makeText(context, context.getString(R.string.please_enter_password), Toast.LENGTH_SHORT).show()
                    }
                    else -> callLogin()
                }
            }
            "toggle" -> {
                if (!rememberMe.get()) {
                    rememberMe.set(true)
                } else {
                    rememberMe.set(false)
                }
            }
        }
    }


    public fun showVerification() {
        val mDialogView = LayoutInflater.from(context).inflate(R.layout.activity_verification, null)
        val mBuilder = AlertDialog.Builder(context).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCanceledOnTouchOutside(true)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val tvSubmitChange: TextView = mDialogView.findViewById<View>(R.id.btNext) as TextView
        val phoneNumber: RadioButton =
            mDialogView.findViewById<View>(R.id.phoneNumber) as RadioButton
        val email2: RadioButton = mDialogView.findViewById<View>(R.id.email) as RadioButton
        val rgOption: RadioGroup = mDialogView.findViewById<RadioGroup>(R.id.rgOption) as RadioGroup

        email2.text = this.email.get()
        phoneNumber.text = this.phone.get()

        tvSubmitChange.setOnClickListener {
              if (phoneNumber.isChecked) {
                "phone"
            } else {
                "email"
            }
           callLogin()
            mAlertDialog.dismiss()
        }
    }


    private fun callLogin() {
        val jsonObject = JSONObject()
        jsonObject.put("email", email.get())
        jsonObject.put("password", password.get())
        val requestBody =
            jsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())
        Log.d("myBody", jsonObject.toString())

        RetrofitCall().callService(
            context,
            true,
            "",
            object : RequestProcess<Response<SigninResponse>> {
                override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                    return retrofitApi.logIn(requestBody)
                }

                override fun onResponse(res: Response<SigninResponse>) {
                    val response = res.body()!!
Log.d("login", "  $response")
                    if (res.isSuccessful && response.success) {
                        CommonMethods.showToast(context, response.message)
                        if (ischecked.get()) {
                            val saveMe = SaveMeData()
                            saveMe.email = email.get()!!
                            saveMe.password = password.get()!!
                            saveMe.isRemember = ischecked.get()
                            PreferenceFile.storeRememberMe(context, saveMe)
                        }

                        //removed type.
                       // response.data.type == "Phone" && !response.data.isPhoneVerified
                       // response.data.type == "Email" && !response.data.isEmailVerified

                        when {
                            !response.data.isPhoneVerified -> {

                                (context as SigninActivity).startActivity(
                                    Intent(context, PhoneVerificationActivity::class.java)
                                        .putExtra("comes", "login")
                                        .putExtra("data", response.data.phone)
                                        .putExtra("isProfileSetUp",response.data.isProfileSetup.toString()))
                            }
                            !response.data.isEmailVerified -> {
                                (context as SigninActivity).startActivity(Intent(context,
                                    EmailVerificationActivity::class.java)
                                    .putExtra("comes", "login")
                                    .putExtra("data", response.data.email)
                                    .putExtra("isProfileSetUp",response.data.isProfileSetup.toString()))
                            }


                            !response.data.isProfileSetup -> {
                                (context as SigninActivity)
                                    .startActivity(Intent(context, ProfileSetupActivity::class.java)
                                    .putExtra("token", response.data.accessToken))
                            }
                            response.data.documentUploadStatus == 0 -> {
                                //no doc uploaded
                                (context as SigninActivity).startActivity(Intent(context, DocumentUploadActivity::class.java).putExtra("token", response.data.accessToken))
                                context.finish()
                            }
                            response.data.documentUploadStatus == 1 -> {
                                //page one doc uploaded
                                (context as SigninActivity).startActivity(Intent(context, UploadVehicleRegistration::class.java).putExtra("token", response.data.accessToken))
                                context.finish()
                            }
                            response.data.documentUploadStatus == 2 -> {
                                //page two doc uploaded
                                (context as SigninActivity).startActivity(Intent(context, UploadingIC::class.java).putExtra("token", response.data.accessToken))
                                context.finish()
                            }

                            else -> {
                                PreferenceFile.storeAuthToken(context, response.data.accessToken)
                                PreferenceFile.storeLoginData(context, response)

                                (context as SigninActivity).startActivity(Intent(context, MainActivity::class.java)
                                )
                                context.finishAffinity()
                            }
                        }


                    } else {
                        CommonMethods.showToast(context, response.message)
                    }
                }



                override fun onException(message: String?) {
                    Log.e("verifyException", "====$message")

                }
            })
    }
}