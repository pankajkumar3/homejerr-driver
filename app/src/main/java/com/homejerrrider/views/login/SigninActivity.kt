package com.homejerrrider.views.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.homejerrrider.R
import com.homejerrrider.databinding.ActivitySigninBinding
import kotlinx.android.synthetic.main.activity_signin.*

class SigninActivity : AppCompatActivity() {
    private var binding:ActivitySigninBinding?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         binding = DataBindingUtil.setContentView(this, R.layout.activity_signin)
        val factory = SigninFactory(this,intent)
        val viewModel = ViewModelProvider(this, factory).get(SigninVM::class.java)
        binding?.viewModel=viewModel

        lsCalSwitch.setOnToggledListener { _, isOn ->
           if (isOn)
           {
               toogleTxt.setTextColor(ResourcesCompat.getColor(this.resources,R.color.purple,this.theme))
           }
            else
           {
               toogleTxt.setTextColor(ResourcesCompat.getColor(this.resources,R.color.hint,this.theme))
           }
        }
    }
}