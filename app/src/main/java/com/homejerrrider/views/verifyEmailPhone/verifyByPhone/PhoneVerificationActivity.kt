package com.homejerrrider.views.verifyEmailPhone.verifyByPhone

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.homejerrrider.R
import com.homejerrrider.databinding.ActivityPhoneVerificationBinding

class PhoneVerificationActivity : AppCompatActivity() {

    private var binding: ActivityPhoneVerificationBinding? = null
    lateinit var vm: PhoneVerificationVM
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_phone_verification)
        val factory = PhoneVerificationFactory(this)
        vm = ViewModelProvider(this, factory).get(PhoneVerificationVM::class.java)
        binding?.vm = vm
        getData()
    }

    private fun getData() {

        if (intent.hasExtra("data")) {
            vm.phone.set(intent.getStringExtra("data"))
            vm.token.set(intent.getStringExtra("token"))

            when (intent.getStringExtra("comes")) {
                "forgot" -> {
                    vm.isForgot.set(true)

                }
                "signup" -> {
                    vm.isForgot.set(false)
                    vm.callSendVerification()
                }
                "login" -> {
                    vm.isForgot.set(false)
                    vm.callSendVerification()
                }
            }
        }
    }


}