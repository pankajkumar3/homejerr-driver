package com.homejerrrider.views.verifyEmailPhone.verifyByEmail.emailverificationresponse

data class EmailVerifyResponse(
    val `data`: Data,
    val message: String,
    val success: Boolean
)