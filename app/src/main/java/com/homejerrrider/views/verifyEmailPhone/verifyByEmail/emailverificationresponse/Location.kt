package com.homejerrrider.views.verifyEmailPhone.verifyByEmail.emailverificationresponse

data class Location(
    val coordinates: List<Double>,
    val type: String
)