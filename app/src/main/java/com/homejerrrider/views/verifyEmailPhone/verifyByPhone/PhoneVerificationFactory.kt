package com.homejerrrider.views.verifyEmailPhone.verifyByPhone

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.homejerrrider.views.verifyEmailPhone.verifyByEmail.EmailVerificationVM

class PhoneVerificationFactory (val context: Context): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PhoneVerificationVM::class.java)) {
            return PhoneVerificationVM(context) as T
        }
        throw IllegalArgumentException("")
    }
}