package com.homejerrrider.views.verifyEmailPhone.verifyByPhone.model

data class Location(
    var coordinates: List<Int>,
    var type: String
)