package com.homejerrrider.views.verifyEmailPhone.verifyByEmail

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.homejerrrider.MainActivity
import com.homejerrrider.networkCalls.RequestProcess
import com.homejerrrider.networkCalls.RetrofitApi
import com.homejerrrider.networkCalls.RetrofitCall
import com.homejerrrider.sharedpreference.PreferenceFile
import com.homejerrrider.utils.CommonMethods
import com.homejerrrider.views.login.signinresponse.SigninResponse
import com.homejerrrider.views.profileSetup.ProfileSetupActivity
import com.homejerrrider.views.resetPassword.ResetPasswordActivity
import com.homejerrrider.views.upLoadDocument.DocumentUploadActivity
import com.homejerrrider.views.uploadingic.UploadingIC
import com.homejerrrider.views.uploadvehicleregistration.UploadVehicleRegistration
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Response

class EmailVerificationVM(val context: Context) : ViewModel() {
    var className = "comes"// No VALUE ASSIGNED HERE. It was missing
    var myEmail = ObservableField("")
    var token = ObservableField("")

    var type = ""
    var isForgot = ObservableBoolean(false)

    var email = ObservableField<String>("")


    fun clicks(value: String) {
        when (value) {
            "cross" -> {
                (context as EmailVerificationActivity).onBackPressed()
            }
            "resendMail" -> {
                if (isForgot.get()) {
                    resend()
                } else {
                    callSendVerification()

                }

            }
            "verifyMail" -> {
                /* if (!isForgot.get()) {
                     callCheckVerification()
                 } else {
                     callCheckVerification()

              }*/
                callCheckVerification()

            }
        }
    }


    fun callSendVerification() {
        val jsonObject = JSONObject()
        jsonObject.put("verificationBy", "email")
        jsonObject.put("email", email.get())


        val requestBody =
            jsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())
        RetrofitCall().callService(
            context,
            true,
            "",
            object : RequestProcess<Response<SigninResponse>> {
                override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                    return retrofitApi.sendVerification(requestBody)
                }

                override fun onResponse(res: Response<SigninResponse>) {
                    val response = res.body()!!
                    if (res.isSuccessful && response.success) {
                        CommonMethods.showToast(context, response.message)

                    } else
                        CommonMethods.showToast(context, response.message)

                }

                override fun onException(message: String?) {
                    Log.e("userException", "====$message")
                }
            })
    }

    fun resend() {
        val jsonObject = JSONObject()
        jsonObject.put("verificationBy", "email")
        jsonObject.put("email", email.get())


        val requestBody =
            jsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())
        RetrofitCall().callService(
            context,
            true,
            "",
            object : RequestProcess<Response<SigninResponse>> {
                override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                    return retrofitApi.resend(requestBody)
                }

                override fun onResponse(res: Response<SigninResponse>) {
                    val response = res.body()!!
                    if (res.isSuccessful && res.body()!!.success) {
                        CommonMethods.showToast(context, response.message)

                    } else
                        CommonMethods.showToast(context, response.message)

                }

                override fun onException(message: String?) {
                    Log.e("userException", "====$message")
                }
            })
    }

    fun callCheckVerification() {
        val jsonObject = JSONObject()
        jsonObject.put("email", email.get())
        val requestBody =
            jsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())
        RetrofitCall().callService(
            context,
            true,
            "",
            object : RequestProcess<Response<SigninResponse>> {
                override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                    return if (isForgot.get()) retrofitApi.checkForgotEmail(requestBody)
                    else retrofitApi.checkEmail(requestBody)
                }

                override fun onResponse(res: Response<SigninResponse>) {
                    val response = res.body()!!
                    if (res.isSuccessful && res.body()!!.success) {
                        //PreferenceFile.storeLoginData(context, response)

                        if (isForgot.get()) {
                            (context as EmailVerificationActivity).startActivity(
                                Intent(
                                    context,
                                    ResetPasswordActivity::class.java
                                ).putExtra("comes", "phone").putExtra("data", email.get())
                            )
                            CommonMethods.showToast(context, response.message)
                            context.finish()
                        } else {
//added by vikas
                            when {

                                // !isForgot.get() &&
                                !response.data.isProfileSetup -> {
                                    //added by vikas
                                    // PreferenceFile.storeLoginData(context, response)

                                    (context as EmailVerificationActivity)
                                        .startActivity(
                                            Intent(
                                                context,
                                                ProfileSetupActivity::class.java
                                            ).putExtra("comes", "email")
                                                .putExtra("data", email.get())
                                                .putExtra("token", token.get())
                                        )
                                    CommonMethods.showToast(context, response.message)
                                    context.finish()

                                }


                                response.data.isProfileSetup -> {
                                    // PreferenceFile.storeLoginData(context, response)
                                    (context as EmailVerificationActivity)
                                        .startActivity(
                                            Intent(
                                                context,
                                                MainActivity::class.java
                                            )
                                                .putExtra("comes", "email")
                                                .putExtra("data", email.get())
                                                .putExtra("token", token.get())
                                        )
                                    CommonMethods.showToast(context, response.message)
                                    context.finish()

                                }
                                response.data.documentUploadStatus == 0 -> {
                                    //no doc uploaded
                                    (context as EmailVerificationActivity).startActivity(
                                        Intent(
                                            context,
                                            DocumentUploadActivity::class.java
                                        ).putExtra("token", response.data.accessToken)
                                    )
                                    context.finish()
                                }
                                response.data.documentUploadStatus == 1 -> {
                                    //page one doc uploaded
                                    (context as EmailVerificationActivity).startActivity(
                                        Intent(
                                            context,
                                            UploadVehicleRegistration::class.java
                                        ).putExtra("token", response.data.accessToken)
                                    )
                                    context.finish()
                                }
                                response.data.documentUploadStatus == 2 -> {
                                    //page two doc uploaded
                                    (context as EmailVerificationActivity)
                                        .startActivity(
                                            Intent(
                                                context,
                                                UploadingIC::class.java
                                            ).putExtra("token", response.data.accessToken)
                                        )
                                    context.finish()
                                }
                                else -> {


                                    PreferenceFile.storeAuthToken(
                                        context,
                                        response.data.accessToken
                                    )
                                    PreferenceFile.storeLoginData(context, response)
                                    (context as EmailVerificationActivity).startActivity(
                                        Intent(
                                            context,
                                            MainActivity::class.java
                                        )
                                    )
                                    context.finishAffinity()
                                }
                            }
                        }
                    } else
                        CommonMethods.showToast(context, response.message)
                }

                override fun onException(message: String?) {
                    Log.e("userException", "====$message")
                }
            })
    }


}
