package com.homejerrrider.views.verifyEmailPhone.verifyByEmail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.homejerrrider.R
import com.homejerrrider.databinding.ActivityEmailVerificationBinding

class EmailVerificationActivity : AppCompatActivity() {
    private var viewModel: EmailVerificationVM? = null
    var binding: ActivityEmailVerificationBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_email_verification)
        val factory = EmailVerificationFactory(this)
        viewModel = ViewModelProvider(this, factory).get(EmailVerificationVM::class.java)
        getData()
        binding?.viewModel = viewModel

    }


    private fun getData() {
        if (intent.hasExtra("data"))
            viewModel?.email?.set(intent.getStringExtra("data"))
            viewModel?.token?.set(intent.getStringExtra("token"))
//            viewModel!!.email.set(intent.getStringExtra("data"))
            when (intent.getStringExtra("comes")) {
                "forgot" -> {
                    viewModel!!.isForgot.set(true)
                }
                "signup" -> {
                    viewModel!!.isForgot.set(false)
                    viewModel!!.callSendVerification()
                }
                "login" -> {
                    viewModel!!.isForgot.set(false)
                    viewModel!!.callSendVerification()
                }
            }
        }
    }



