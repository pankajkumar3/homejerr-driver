package com.homejerrrider.views.verifyEmailPhone.verifyByPhone

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.homejerrrider.MainActivity
import com.homejerrrider.networkCalls.RequestProcess
import com.homejerrrider.networkCalls.RetrofitApi
import com.homejerrrider.networkCalls.RetrofitCall
import com.homejerrrider.sharedpreference.PreferenceFile
import com.homejerrrider.utils.CommonMethods
import com.homejerrrider.utils.CommonMethods.showToast
import com.homejerrrider.views.forgot.model.ForgotResponse
import com.homejerrrider.views.login.SigninActivity
import com.homejerrrider.views.login.signinresponse.SigninResponse
import com.homejerrrider.views.profileSetup.ProfileSetupActivity
import com.homejerrrider.views.resetPassword.ResetPasswordActivity
import com.homejerrrider.views.upLoadDocument.DocumentUploadActivity
import com.homejerrrider.views.uploadingic.UploadingIC
import com.homejerrrider.views.uploadvehicleregistration.UploadVehicleRegistration
import com.homejerrrider.views.verifyEmailPhone.verifyByEmail.EmailVerificationActivity
import com.mukesh.mukeshotpview.completeListener.MukeshOtpCompleteListener
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Response

class PhoneVerificationVM(val context: Context) : ViewModel() {
    var phone = ObservableField("")
    var otp = ObservableField("")

    var token = ObservableField("")
    val isForgot by lazy { ObservableBoolean(false) }
    var isForgotIs = ObservableBoolean(false)


    val otpListener by lazy {
        object : MukeshOtpCompleteListener {

            override fun otpCompleteListener(otpIs: String?) {

                otp.set(otpIs)
                if (validOtp()) {
                    if (!isForgot.get())
                        callVerifyOTP()
                    else
                        callForgotVerifyOTP()
                }
            }
        }
    }


    private fun validOtp(): Boolean {
        when {
            otp.get().toString().trim().isEmpty() || otp.get().toString().trim().length < 4 -> {
                showToast(context, "Please enter valid OTP.")
                return false
            }
        }
        return true

    }

    fun clicks(value: String) {
        when (value) {
            "cross" -> {
                (context as PhoneVerificationActivity).startActivity(Intent(context,SigninActivity::class.java))
            }
            "resendPhone" -> {
                callSendVerification()
                // callResend()

            }
        }
    }


    private fun callResend() {

        val jsonElement = JsonObject()
        jsonElement.addProperty("email", phone.get())

        try {
            RetrofitCall().callService(
                context,
                true,
                "",
                object : RequestProcess<Response<ForgotResponse>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<ForgotResponse> {
                        return retrofitApi.resendVerification(jsonElement)
                    }

                    override fun onResponse(res: retrofit2.Response<ForgotResponse>) {
                        val response = res.body()!!
                        if (res.isSuccessful) {
                            if (response.success) {
                                showToast(context, response.message)
                            } else {
                                CommonMethods.showToast(context, response.message)
                            }
                        }
                    }

                    override fun onException(message: String?) {
                        CommonMethods.showToast(context, "$message")
                    }
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun callSendVerification() {
        val jsonObject = JSONObject()
        jsonObject.put("verificationBy", "phone")
        jsonObject.put("phone", phone.get())
        val requestBody =
            jsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())
        RetrofitCall().callService(
            context,
            true,
            "",
            object : RequestProcess<Response<SigninResponse>> {
                override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                    return retrofitApi.sendVerification(requestBody)
                }

                override fun onResponse(res: Response<SigninResponse>) {
                    val response = res.body()!!
                    if (res.isSuccessful && res.body()!!.success) {
                        CommonMethods.showToast(context, res.body()!!.message)
                    } else {
                        CommonMethods.showToast(context, res.body()!!.message)
                    }
                }

                override fun onException(message: String?) {
                    Log.e("userException", "====$message")
                }
            })
    }

    fun callVerifyOTP() = try {

        val objectJson = JsonObject()
        objectJson.addProperty("otp", otp.get())
        objectJson.addProperty("phone", phone.get())

        RetrofitCall().callService(
            context,
            true,
            "",
            object : RequestProcess<Response<SigninResponse>> {
                override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                    return retrofitApi.verifyPhone(objectJson)
                }

                override fun onResponse(res: Response<SigninResponse>) {
                    val response = res.body()!!
                    if (res.isSuccessful && response.success)
                    {
                        showToast(context, res.body()!!.message)

                      /*  if(!response.data.isProfileSetup){
                            (context as PhoneVerificationActivity).startActivity(Intent(context, ProfileSetupActivity::class.java).putExtra("token", response.data.accessToken))

                        }

                        else
                            (context as PhoneVerificationActivity).startActivity(Intent(context, MainActivity::class.java).putExtra("token", response.data.accessToken))
*/
                        when {

                            response.data.isProfileSetup->{

                               // PreferenceFile.storeLoginData(context, response)

                                (context as PhoneVerificationActivity)
                                    .startActivity(Intent(context,
                                        MainActivity::class.java)
                                        .putExtra("token", response.data.accessToken)
                                        )
                                CommonMethods.showToast(context, response.message)
                                context.finish()
                            }

                           !response.data.isProfileSetup->{

                             //  PreferenceFile.storeAuthToken(context, response.data.accessToken)
                                (context as PhoneVerificationActivity)
                                    .startActivity(Intent(context,
                                        ProfileSetupActivity::class.java)
                                        .putExtra("token", response.data.accessToken))
                                CommonMethods.showToast(context, response.message)
                                context.finish()
                            }

                            response.data.documentUploadStatus == 0 -> {
                                //no doc uploaded
                                (context as PhoneVerificationActivity)
                                    .startActivity(Intent(context, DocumentUploadActivity::class.java).putExtra("token", response.data.accessToken))
                                context.finish()
                            }
                            response.data.documentUploadStatus == 1 -> {
                                //page one doc uploaded
                                (context as PhoneVerificationActivity).startActivity(
                                    Intent(context, UploadVehicleRegistration::class.java
                                    ).putExtra("token", response.data.accessToken)
                                )
                                context.finish()
                            }
                            response.data.documentUploadStatus == 2 -> {
                                //page two doc uploaded
                                (context as PhoneVerificationActivity)
                                    .startActivity(Intent(context, UploadingIC::class.java).putExtra("token", response.data.accessToken))
                                context.finish()
                            }

                            else -> {

                                PreferenceFile.storeAuthToken(context, response.data.accessToken)
                                PreferenceFile.storeLoginData(context, response)

                                (context as PhoneVerificationActivity).startActivity(Intent(context, MainActivity::class.java))
                                context.finishAffinity()
                            }
                        }


                    }
                    else
                    {
                        showToast(context, res.body()!!.message)

                    }
                }

                override fun onException(message: String?) {
                    Log.e("verifyException", "====$message")
                }
            })

    } catch (e: Exception) {
        e.printStackTrace()
    }

    fun callForgotVerifyOTP() = try {

        val objectJson = JsonObject()
        objectJson.addProperty("phone", phone.get())

        objectJson.addProperty("otp", otp.get())

        RetrofitCall().callService(
            context,
            true,
            "",
            object : RequestProcess<Response<SigninResponse>> {
                override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                    return retrofitApi.verifyPhoneForgot(objectJson)
                }

                override fun onResponse(res: Response<SigninResponse>) {
                    val response = res.body()!!
                    if (res.isSuccessful) {
                        if (response.success) {
                            showToast(context, response.message)
                            // PreferenceFile.storeAuthToken(context, response.data.accessToken)
                            // PreferenceFile.storeLoginData(context, response)


                            (context as PhoneVerificationActivity).startActivity(
                                Intent(
                                    context,
                                    ResetPasswordActivity::class.java
                                ).putExtra("comes", "phone").putExtra("data", phone.get())
                            )
                            context.finishAffinity()
                        }
                    } else {
                        showToast(context, response.message)

                    }
                }

                override fun onException(message: String?) {
                    Log.e("verifyException", "====$message")
                }
            })

    } catch (e: Exception) {
        e.printStackTrace()
    }
}