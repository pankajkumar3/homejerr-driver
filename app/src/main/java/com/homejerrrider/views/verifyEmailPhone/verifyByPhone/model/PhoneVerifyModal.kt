package com.homejerrrider.views.verifyEmailPhone.verifyByPhone.model

data class PhoneVerifyModal(
    var `data`: Data,
    var message: String,
    var success: Boolean
)