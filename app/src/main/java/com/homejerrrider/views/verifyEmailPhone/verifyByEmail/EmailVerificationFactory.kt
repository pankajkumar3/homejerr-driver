package com.homejerrrider.views.verifyEmailPhone.verifyByEmail

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class EmailVerificationFactory  (val context: Context): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EmailVerificationVM::class.java)) {
            return EmailVerificationVM(context) as T
        }
        throw IllegalArgumentException("")
    }
}