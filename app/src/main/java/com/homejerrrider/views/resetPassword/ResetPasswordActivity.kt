package com.homejerrrider.views.resetPassword

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.homejerrrider.R
import com.homejerrrider.databinding.ActivityResetPasswordBinding

class ResetPasswordActivity : AppCompatActivity() {
    var vm: ResetPasswordVM? = null
    private var binding: ActivityResetPasswordBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reset_password)
        val factory = ResetPasswordFactory(this, intent)
        vm = ViewModelProvider(this, factory).get(ResetPasswordVM::class.java)
        binding?.vm = vm
       getData()
    }


    private fun getData() {
        if (intent.hasExtra("comes") && intent.getStringExtra("comes").equals("phone")){
            vm?.email!!.set(intent.getStringExtra("data"))
            vm?.isPhone?.set(true)
        }
        else vm?.isPhone?.set(false)


    }

    override fun onResume() {
        super.onResume()
    }
}