package com.homejerrrider.views.resetPassword

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.homejerrrider.views.login.SigninVM

class ResetPasswordFactory(val context: Context, val intent: Intent): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ResetPasswordVM::class.java)) {
            return ResetPasswordVM(context, intent) as T
        }
        throw IllegalArgumentException("")
    }
}