package com.homejerrrider.views.resetPassword

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.homejerrrider.R
import com.homejerrrider.networkCalls.RequestProcess
import com.homejerrrider.networkCalls.RetrofitApi
import com.homejerrrider.networkCalls.RetrofitCall
import com.homejerrrider.utils.CommonMethods
import com.homejerrrider.utils.CommonMethods.showToast
import com.homejerrrider.views.forgot.model.ForgotResponse
import com.homejerrrider.views.login.SigninActivity
import com.homejerrrider.views.login.signinresponse.SigninResponse
import retrofit2.Response

class ResetPasswordVM(val context: Context, val intent: Intent) : ViewModel() {
    val password = ObservableField("")
    private val conPassword = ObservableField("")
    var isPhone = ObservableBoolean(false)
    var email = ObservableField("")
    var confirmPassword = ObservableField("")
    var type = ""

    // var email = ""
   // var phone = ""

/*
    init {
        if (intent.hasExtra("email")) {
            email = intent.getStringExtra("email")!!
        }
        if (intent.hasExtra("phone")) {
            phone = intent.getStringExtra("phone")!!
            type = "Phone"
        }
    }
*/

    fun clicks(value: String) {
        when (value) {
            "submit" -> {
                if (validation()){
                    resetPassword()

                }
            }
        }
    }

    private fun validation(): Boolean {
        when {
            password.get()!!.trim() == "" -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterPassword))
                return false
            }
            password.get()!!.trim().length < 8 -> {
                CommonMethods.showToast(context, "You must have 8 characters in your password.")
                return false
            }
            confirmPassword.get()!!.trim() == "" -> {
                CommonMethods.showToast(context, "Please enter confirm password.")
                return false
            }


            confirmPassword.get()!!.trim().length < 8 -> {
                CommonMethods.showToast(context, "You must have 8 characters in your password.")
                return false
            }
            !password.get().equals(confirmPassword.get()!!) || confirmPassword.get()!!
                .isEmpty() -> {
                showToast(context, context.getString(R.string.passwordNotMatch))
                return false
            }

        }
        return true
    }

    private fun resetPassword() {
        val objectReset = JsonObject()
        if (email.get()!!.contains("@")) {
            objectReset.addProperty("email", email.get()!!)
        } else {
            objectReset.addProperty("phone", email.get()!!)
        }
        objectReset.addProperty("reset_password", password.get())
        objectReset.addProperty("confirm_password", confirmPassword.get())


        try {
            RetrofitCall().callService(
                context,
                true,
                "",
                object : RequestProcess<Response<SigninResponse>> {
                    override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SigninResponse> {
                        return retrofitApi.resetPassword(
                            //authorization.get()!!,
                            objectReset
                        )
                    }

                    override fun onResponse(res: Response<SigninResponse>) {
                        val response = res.body()
                        if (res.body()?.success!!) {
                            showToast(context, res.body()!!.message)
                            (context as ResetPasswordActivity).startActivity(Intent(context, SigninActivity::class.java)
                            )
                            context.finish()
                            // context
                        } else {
                            showToast(context, res.body()!!.message)
                        }
                    }

                    override fun onException(message: String?) {
                        Log.e("userException", "====$message")
                    }
                })

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}