package com.homejerrrider.views.walkThrough

import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.homejerrrider.R
import com.homejerrrider.databinding.ActivityWalkThroughBinding

class WalkThroughActivity : AppCompatActivity() {
    private var walkThroughBinding: ActivityWalkThroughBinding? = null
    var vm: WalkthroughVM? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        walkThroughBinding = DataBindingUtil.setContentView(this, R.layout.activity_walk_through)
        val factory = WalkThroughFactory(this, walkThroughBinding!!)
        vm = ViewModelProvider(this, factory).get(WalkthroughVM::class.java)
        walkThroughBinding?.vm = vm
        timer()
    }


    private fun timer() {
        var timer = object : CountDownTimer(6000, 2000) {
            override fun onTick(millisUntilFinished: Long) {
                Log.e("timer", "onTick: $millisUntilFinished")
                when (millisUntilFinished) {
                    in 5901..6000 -> {
                        vm!!.pagerNext(walkThroughBinding!!.vpWalkThrough)
                    }
                    in 3001..4000 -> {
                        vm!!.pagerNext(walkThroughBinding!!.vpWalkThrough)

                    }
                    in 1900..2000 -> {
                        vm!!.pagerNext(walkThroughBinding!!.vpWalkThrough)

                    }
                }
                // timerTextIs.set("00:" + (millisUntilFinished / 1000).toString() + " seconds")
            }

            override fun onFinish() {
                //  timerBoolean.set(false)
                cancel()
            }

        }
        timer.start()
    }

}