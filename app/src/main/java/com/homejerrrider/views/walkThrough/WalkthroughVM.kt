package com.homejerrrider.views.walkThrough

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import androidx.viewpager.widget.ViewPager
import com.homejerrrider.R
import com.homejerrrider.databinding.ActivityWalkThroughBinding
import com.homejerrrider.views.chooseLoginSignup.ChooseLoginSignupActivity
import com.homejerrrider.views.walkThrough.adapter.WalkThroughVP

class WalkthroughVM(context: Context, walkThroughBinding: ActivityWalkThroughBinding) : ViewModel() {

    var mContext = context
    var btName = ObservableField("Next")
    var skipVisibility = ObservableBoolean(true)
    var arrayList = arrayListOf(
        R.layout.walk_through_one,
        R.layout.walk_through_two,
        R.layout.walk_through_three
    )
    var wtViewPager = WalkThroughVP(context, arrayList)


    var pagerListener = walkThroughBinding.vpWalkThrough.addOnPageChangeListener(object :
        ViewPager.OnPageChangeListener {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
            when (position) {
                2 -> {
                    btName.set("Next")
                }
                else -> {
                    btName.set("Next")
                }
            }
        }

        override fun onPageSelected(position: Int) {}
        override fun onPageScrollStateChanged(state: Int) {}
    })

    init {
        pagerListener
    }

    fun pagerSkip() {
        (mContext as WalkThroughActivity).startActivity(
            Intent(
                mContext,
                ChooseLoginSignupActivity::class.java
            )
        )
        (mContext as WalkThroughActivity).finish()
    }

    fun pagerNext(viewPager: ViewPager) {
        val selected = viewPager.currentItem
        Log.e("selected", (selected + 1).toString())
        when (selected) {
            2 -> {
                pagerSkip()
            }
            else -> {
                viewPager.currentItem = selected + 1
            }
        }
    }
}
