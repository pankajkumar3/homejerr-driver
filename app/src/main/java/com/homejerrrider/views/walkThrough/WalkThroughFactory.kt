package com.homejerrrider.views.walkThrough

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.homejerrrider.databinding.ActivityWalkThroughBinding

class WalkThroughFactory(val context: Context, val walkThroughBinding: ActivityWalkThroughBinding) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        if (modelClass.isAssignableFrom(WalkthroughVM::class.java))
        {
            return WalkthroughVM(context,walkThroughBinding) as T
        }
        throw IllegalArgumentException("")
    }
}