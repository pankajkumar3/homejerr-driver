package com.homejerrrider.views.chooseLoginSignup

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.homejerrrider.R
import com.homejerrrider.databinding.ActivityChooseLoginSignupBinding
import com.homejerrrider.databinding.ActivitySigninBinding
import com.homejerrrider.views.login.SigninFactory
import com.homejerrrider.views.login.SigninVM

class ChooseLoginSignupActivity : AppCompatActivity() {
    private var signUp: ActivityChooseLoginSignupBinding?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signUp = DataBindingUtil.setContentView(this, R.layout.activity_choose_login_signup)
        val factory = ChooseLoginSignupFactory(this,intent)
        val viewModel = ViewModelProvider(this, factory).get(ChooseLoginSignupVM::class.java)
        signUp?.viewModel=viewModel
    }
}