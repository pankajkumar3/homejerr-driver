package com.homejerrrider.views.chooseLoginSignup

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ChooseLoginSignupFactory (val context: Context, val intent: Intent) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ChooseLoginSignupVM::class.java)) {
            return ChooseLoginSignupVM(context,intent) as T
        }
        throw IllegalArgumentException("")
    }
}