package com.homejerrrider.views.chooseLoginSignup

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.homejerrrider.views.login.SigninActivity
import com.homejerrrider.views.signup.SignupActivity

class ChooseLoginSignupVM(val context: Context, val intent: Intent): ViewModel() {
    var type = "signup"
    var signin = ObservableField("Sign In")
    var signup = ObservableField("Sign up")

    init
    {
        if (intent.hasExtra("type"))
        {
            if (intent.getStringExtra("type") == "signup")
            {
                type = "signup"
                signup.set("Sign Up")
            }
            else
            {
                type = "signin"
                signup.set("Sign In")
            }

            Log.e("signupIntent",intent.getStringExtra("type")!!)
        }
    }

    fun clicks(value: String) {
        when (value) {
            "signIN" -> {
                context.startActivity(
                    Intent(context, SigninActivity::class.java)
                        .putExtra("signtype", "signin")
                )
            }
            "signUP" -> {
                context.startActivity(
                    Intent(context, SignupActivity::class.java)
                        .putExtra("signtype", "signup")
                )

            }
        }
    }
}
