package com.homejerrrider.views.signup

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SignupFactory  (val context: Context, val intent: Intent): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SignUpNewVM::class.java)) {
            return SignUpNewVM(context, intent) as T
        }
        throw IllegalArgumentException("")
    }
}