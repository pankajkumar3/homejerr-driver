package com.homejerrrider.views.signup.model

data class SignUpModal(
    var `data`: Data,
    var message: String,
    var success: Boolean
)