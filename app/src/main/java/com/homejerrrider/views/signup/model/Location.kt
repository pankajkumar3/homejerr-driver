package com.homejerrrider.views.signup.model

data class Location(
    var coordinates: List<Double>,
    var type: String
)