package com.homejerrrider.views.signup

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.homejerrrider.R
import com.homejerrrider.networkCalls.RequestProcess
import com.homejerrrider.networkCalls.RetrofitApi
import com.homejerrrider.networkCalls.RetrofitCall
import com.homejerrrider.sharedpreference.PreferenceFile
import com.homejerrrider.sharedpreference.PreferenceKeys
import com.homejerrrider.utils.CommonMethods
import com.homejerrrider.validate.ValidatorUtils
import com.homejerrrider.views.login.SigninActivity
import com.homejerrrider.views.signup.model.SignUpModal
import com.homejerrrider.views.verifyEmailPhone.verifyByEmail.EmailVerificationActivity
import com.homejerrrider.views.verifyEmailPhone.verifyByPhone.PhoneVerificationActivity
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Response


class SignUpNewVM(val context: Context, val intent: Intent) : ViewModel() {


    val email = ObservableField("")
    val phone = ObservableField("")
    val password = ObservableField("")
    val conPassword = ObservableField("")
    var typecheck = ""

    fun clicks(value: String) {
        when (value) {
            "login" -> {
                (context as SignupActivity).startActivity(
                    Intent(
                        context,
                        SigninActivity::class.java
                    )
                )
            }
            "back" -> {
                (context as SignupActivity).onBackPressed()
            }
            "signUP" -> {
                if (signUpValidation()) {
                    showVerification()

                }

            }
        }
    }

    public fun signAPI() {
        val jsonObject = JSONObject()
        jsonObject.put("email", email.get())
        jsonObject.put("phone", phone.get())
        jsonObject.put("password", password.get())
        jsonObject.put("confirmPassword", conPassword.get())
        val requestBody =
            jsonObject.toString().toRequestBody("application/json".toMediaTypeOrNull())
        RetrofitCall().callService(
            context,
            true,
            "",
            object : RequestProcess<Response<SignUpModal>> {
                override suspend fun sendRequest(retrofitApi: RetrofitApi): Response<SignUpModal> {
                    return retrofitApi.signUp(requestBody)
                }

                override fun onResponse(response: Response<SignUpModal>) {

                    if (response.isSuccessful && response.body()!!.success) {

                        CommonMethods.showToast(context, response.body()!!.message)
                            if (response.body()!!.success) {
                                if (typecheck == "phone") {
                                    (context as SignupActivity).startActivity(Intent(context, PhoneVerificationActivity::class.java).putExtra("comes", "signup").putExtra("data", phone.get()).putExtra("token",response.body()!!.data.accessToken))
                                    // (context as SigninActivity).finish()
                                } else {
                                    (context as SignupActivity).startActivity(Intent(context, EmailVerificationActivity::class.java).putExtra("comes", "signup").putExtra("data", email.get()).putExtra("token",response.body()!!.data.accessToken))
                                }
                            }
                            (context as SignupActivity).finish()

                        }
                        else {
                            CommonMethods.showToast(context, response.body()!!.message)
                        }

                }

                override fun onException(message: String?) {

                    Log.e("userException", "====$message")

                }
            })
    }

    fun signUpValidation(): Boolean {
        when {
            email.get()!!.trim() == "" -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterEmail))
                return false
            }
            !ValidatorUtils.isEmailValid(email.get()!!.trim()) -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterValidEmail))
                return false
            }
            phone.get()!!.trim() == "" -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterMobileNo))
                return false
            }
            !ValidatorUtils.isMobileValid(phone.get()!!.trim()) -> {
                CommonMethods.showToast(context, context.getString(R.string.enterValidMobileNo))
                return false
            }
            password.get()!!.trim() == "" -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterPassword))
                return false
            }
            password.get()!!.trim().length < 8 -> {
                CommonMethods.showToast(context, context.getString(R.string.passwordMustHave8Char))
                return false
            }
            conPassword.get()!!.trim() == "" -> {
                CommonMethods.showToast(context, context.getString(R.string.pleaseEnterConfirmPss))
                return false
            }
            conPassword.get()!!.trim() != password.get()!!.trim() -> {
                CommonMethods.showToast(context, context.getString(R.string.passwordNotMatch))
                return false
            }
        }
        return true
    }

    public fun showVerification() {
        val mDialogView = LayoutInflater.from(context).inflate(R.layout.activity_verification, null)
        val mBuilder = AlertDialog.Builder(context).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCanceledOnTouchOutside(true)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val tvSubmitChange: TextView = mDialogView.findViewById<View>(R.id.btNext) as TextView
        val phoneNumber: RadioButton =
            mDialogView.findViewById<View>(R.id.phoneNumber) as RadioButton
        val email2: RadioButton = mDialogView.findViewById<View>(R.id.email) as RadioButton
        val rgOption: RadioGroup = mDialogView.findViewById<RadioGroup>(R.id.rgOption) as RadioGroup

        email2.text = this.email.get()
        phoneNumber.text = this.phone.get()

        tvSubmitChange.setOnClickListener {
            typecheck = if (phoneNumber.isChecked) {
                "phone"
            } else {
                "email"
            }
            signAPI()
            mAlertDialog.dismiss()
        }
    }
}


