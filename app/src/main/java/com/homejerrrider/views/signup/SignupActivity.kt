package com.homejerrrider.views.signup

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.homejerrrider.R
import com.homejerrrider.databinding.ActivitySignupBinding

class SignupActivity : AppCompatActivity() {
    private var binding: ActivitySignupBinding?=null
    var vm:SignUpNewVM?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup)
        val factory = SignupFactory(this,intent)
         vm = ViewModelProvider(this, factory).get(SignUpNewVM::class.java)
        binding?.vm=vm
    }
}