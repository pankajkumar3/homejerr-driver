package com.homejerrrider.views.uploadvehicleregistration

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class UploadingVehicleRegFactory(val context: Context, val intent: Intent): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UploadVehicleRegistrationVM::class.java)) {
            return UploadVehicleRegistrationVM(context, intent) as T
        }
        throw IllegalArgumentException("")
    }
}