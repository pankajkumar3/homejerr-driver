package com.homejerrrider.views.welcomePage

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.homejerrrider.MainActivity
import com.homejerrrider.R
import com.homejerrrider.views.walkThrough.WalkThroughActivity

class WelcomePage:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.welcome_dialog)
        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(
                    Intent(this, MainActivity::class.java)
            )
            finishAffinity()

        }, 3000)
    }
}

