package com.homejerrrider.networkCalls

interface RequestProcess<T>
{
    suspend fun sendRequest(retrofitApi: RetrofitApi):T

    fun onResponse(res: T)

    fun onException(message: String?)

}