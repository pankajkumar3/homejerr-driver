package com.homejerrrider.networkCalls

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.homejerrrider.views.forgot.model.ForgotResponse
import com.homejerrrider.views.fragments.editprofile.editresponse.EditProfileResponse
import com.homejerrrider.views.login.signinresponse.SigninResponse
import com.homejerrrider.views.signup.model.SignUpModal
import com.homejerrrider.views.upLoadDocument.uploaddocresponse.DocumentsResponse
import com.homejerrrider.views.verifyEmailPhone.verifyByEmail.emailverificationresponse.EmailVerifyResponse
import com.homejerrrider.views.verifyEmailPhone.verifyByPhone.model.PhoneVerifyModal
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface RetrofitApi {
    @POST(WebApiKey.SIGN_UP)
    suspend fun signUp(@Body addSignUp: RequestBody): Response<SignUpModal>

    @POST(WebApiKey.LOGIN)
    suspend fun logIn(@Body addLogin: RequestBody): Response<SigninResponse>

  /*  @POST(WebApiKey.FORGOT_PASS)
    suspend fun forgotPassword(@Body forgot: JsonObject): Response<JsonElement>
*/
    @POST(WebApiKey.FORGOT_PASS)
    suspend fun forgotPassword(@Body jsonObj: JsonObject): Response<SigninResponse>
  @POST(WebApiKey.RESEND_PHONE)
    suspend fun resendVerification(@Body jsonObj: JsonObject): Response<ForgotResponse>



    @POST(WebApiKey.RESET)
    suspend fun resetPassword(@Body forgot: JsonObject): Response<SigninResponse>

    @Multipart
    @POST(WebApiKey.UPLOAD_DOCS)
    suspend fun uploadDocs(
        @PartMap body: HashMap<String, RequestBody>,
        @Part part: MultipartBody.Part?,
        @Part part1: MultipartBody.Part?
    ): Response<SigninResponse>


    @POST(WebApiKey.CHECK_FORGOT)
    suspend fun checkForgot(@Body forgot: RequestBody): Response<JsonElement>

    @POST(WebApiKey.VERIFY_FORGOT)
    suspend fun verifyForgotPassword(@Body forgot: RequestBody): Response<JsonElement>

    @POST(WebApiKey.VERIFY_PHONE)
    suspend fun verifyPhone(@Body forgot: JsonObject): Response<SigninResponse>

 @POST(WebApiKey.VERIFY_PHONE_FORGOT)
    suspend fun verifyPhoneForgot(@Body forgot: JsonObject): Response<SigninResponse>

    @POST(WebApiKey.SEND_OTP)
    suspend fun sendOtp(@Body forgot: RequestBody): Response<JsonElement>

    @POST(WebApiKey.RESEND_EMAIL)
    suspend fun resendEmail(@Body forgot: RequestBody): Response<JsonElement>

    @POST(WebApiKey.CHECK_EMAIL)
    suspend fun checkEmail(@Body forgot: RequestBody): Response<SigninResponse>


    @POST(WebApiKey.CHECK_FORGOT_EMAIL)
    suspend fun checkForgotEmail(@Body forgot: RequestBody): Response<SigninResponse>



    @POST(WebApiKey.SEND_EMAIL)
    suspend fun sendVerification(@Body forgot: RequestBody): Response<SigninResponse>

@POST(WebApiKey.SEND_FORGOT_EMAIL_RESEND)
    suspend fun resend(@Body forgot: RequestBody): Response<SigninResponse>

    @Multipart
    @POST(WebApiKey.PROFILE_EDIT)
    suspend fun profileEdit(
        @PartMap body: HashMap<String, RequestBody>,
        @Part part: MultipartBody.Part?
    ): Response<SigninResponse>

    @Multipart
    @POST(WebApiKey.PROFILE_SETUP)
    suspend fun profileSetup(
        @PartMap body: HashMap<String, RequestBody>,
        @Part part: MultipartBody.Part?
    ): Response<SigninResponse>

    @Multipart
    @POST(WebApiKey.UPLOAD_DOCS)
    suspend fun uploadDocuments(@Part part: ArrayList<MultipartBody.Part?>): Response<JsonElement>
}