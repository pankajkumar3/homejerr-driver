package com.homejerrrider.networkCalls

class WebApiKey {

    companion object {
        const val BASE_URL = "http://15.207.74.128:3001/driver/"
        const val BASE_URL_IMAGE = "http://15.207.74.128:3001/"

        const val SIGN_UP = "signup"
        const val LOGIN = "signin"

        const val VERIFY_PHONE = "verifyPhone"
        const val VERIFY_PHONE_FORGOT = "verifyForgetPassword"

        const val RESEND_EMAIL = "resendVerification"
        const val CHECK_EMAIL = "checkVerification"
        const val CHECK_FORGOT_EMAIL = "checkForgetVerification"
        const val SEND_EMAIL = "sendVerification"
        const val SEND_FORGOT_EMAIL_RESEND = "resendForgetEmail"

        const val PROFILE_SETUP = "profileSetup"
        const val PROFILE_EDIT = "editProfile"
        const val UPLOAD_DOCS = "uploadDocs"
        const val FORGOT_PASS = "forgetPass"
        const val RESEND_PHONE = "resendVerification"

        const val VERIFY_FORGOT = "verifyForgetPassword"
        const val CHECK_FORGOT = "checkForgetVerification"
        const val SEND_OTP = "sendOtpAgain"
        const val RESET = "resetPassword"
    }
}