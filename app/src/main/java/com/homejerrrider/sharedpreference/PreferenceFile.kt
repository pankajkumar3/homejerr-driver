package com.homejerrrider.sharedpreference

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.homejerrrider.model.SaveMeData
import com.homejerrrider.sharedpreference.PreferenceKeys.token
import com.homejerrrider.views.login.signinresponse.SigninResponse

object PreferenceFile {
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var foreverSharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor


    fun storeRememberMe(context: Context, rememberMeData: SaveMeData) {
        foreverSharedPreferences =
            context.getSharedPreferences(PreferenceKeys.foreverPreferenceName, Context.MODE_PRIVATE)
        val prefsEditor: SharedPreferences.Editor = foreverSharedPreferences.edit()
        prefsEditor.putString(PreferenceKeys.rememberMeData, Gson().toJson(rememberMeData))
        prefsEditor.apply()
        prefsEditor.commit()
    }

    fun storeKey(context: Context, key: String, value: String) {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
        editor.commit()
    }


    fun retrieveAuthToken(context: Context): String? {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        return sharedPreferences.getString(token, null)
    }

    fun clearForeverPreference(context: Context) {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.foreverPreferenceName, Context.MODE_PRIVATE)
        editor = foreverSharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    fun retrieveLoginData(context: Context): SigninResponse? {
        sharedPreferences = context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        return Gson().fromJson(sharedPreferences.getString(PreferenceKeys.loginData, "")!!,
            SigninResponse::class.java
        )
    }

    fun retrieveKey(context: Context, key: String): String? {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, null)
    }

    fun storeBoolean(context: Context, key: String, value: Boolean) {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun retrieveBooleanKey(context: Context, key: String): Boolean? {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean(key, false)
    }


    fun storeLoginData(context: Context, signResponse: SigninResponse) {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        val prefsEditor: SharedPreferences.Editor = sharedPreferences.edit()
        prefsEditor.putString(PreferenceKeys.loginData, Gson().toJson(signResponse))
        prefsEditor.apply()
        prefsEditor.commit()
    }


    fun storeAuthToken(context: Context, authToken: String) {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        val prefsEditor: SharedPreferences.Editor = sharedPreferences.edit()
//        prefsEditor.putString(PreferenceKeys.Bearer + token, authToken)
        prefsEditor.putString(token, authToken)
        prefsEditor.apply()
        prefsEditor.commit()
    }

    fun clearPreference(context: Context) {
        sharedPreferences =
            context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    fun retrieveRememberMe(context: Context): SaveMeData? {
        foreverSharedPreferences =
            context.getSharedPreferences(PreferenceKeys.foreverPreferenceName, Context.MODE_PRIVATE)
        return Gson().fromJson(
            foreverSharedPreferences.getString(
                PreferenceKeys.rememberMeData, ""
            )!!, SaveMeData::class.java
        )
    }

    fun storeForeverKey(context: Context, key: String, value: String) {
        foreverSharedPreferences =
            context.getSharedPreferences(PreferenceKeys.foreverPreferenceName, Context.MODE_PRIVATE)
        editor = foreverSharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()

    }

    fun retrieveForeverKey(context: Context, key: String): String? {
        foreverSharedPreferences =
            context.getSharedPreferences(PreferenceKeys.foreverPreferenceName, Context.MODE_PRIVATE)
        return foreverSharedPreferences.getString(key, null)
    }
}
