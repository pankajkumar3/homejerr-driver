package com.homejerrrider.sharedpreference

import android.content.Context
import android.content.SharedPreferences

object PreferenceKeys {
    var preferenceName = "com.homejerrr"
    const val foreverPreferenceName = "HomeJerrForever"

    var welcome="welcome"
    var email = "email"
    var pass = "pass"
    var code = "code"
    var loginData = "loginData"
    const val rememberMeData = "rememberMeData"
    const val token ="token"
    const val phone = "phone"
}